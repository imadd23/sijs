<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_controller extends CI_Controller {

	protected $ci;
	protected $openController = ['welcome', 'auth', 'beranda'];
	protected $controller;
	protected $method;
	protected $_authAction;

	public function __construct(){
		parent::__construct();
		$this->init();
	}

	public function init(){
		$this->controller = $this->router->fetch_class();
		$this->method = $this->router->fetch_method();
	}

	public function letMeToAccess($authAction = ''){
		// get property from controller running
		$this->_authAction = $authAction;
		if (!in_array($this->controller, $this->openController)){
			$this->load->library(['session']);
			$role_id = is_null($this->session->userdata('role_id')) ? 0 : $this->session->userdata('role_id');
			$qAuth = $this->db->select('*')->from('auth_role_page')
				->join('auth_page', 'auth_page.page_id = auth_role_page.page_id', 'left')
				->join('auth_role', 'auth_role.role_id = auth_role_page.page_id', 'left')
				->where(['auth_role_page.role_id' => $role_id, 'page' => $this->controller, 'action' => $this->_authAction])
				->get()->num_rows();
			$hasAuthenticated = ($qAuth > 0) ? true : false;
			if (!$hasAuthenticated){
				die('Sorry, This system restrict your access for this page');
			}
		}		
		return true;
	}

	public function has_access($controller, $action){
		$this->_authAction = $action;
		if (!in_array($controller, $this->openController)){
			$this->load->library(['session']);
			$role_id = is_null($this->session->userdata('role_id')) ? 0 : $this->session->userdata('role_id');
			$qAuth = $this->db->select('*')->from('auth_role_page')
				->join('auth_page', 'auth_page.page_id = auth_role_page.page_id', 'left')
				->join('auth_role', 'auth_role.role_id = auth_role_page.page_id', 'left')
				->where(['auth_role_page.role_id' => $role_id, 'page' => $controller, 'action' => $this->_authAction])
				->get()->num_rows();
			$hasAuthenticated = ($qAuth > 0) ? true : false;
			if (!$hasAuthenticated){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}

	public function render_view($view = '', $params = false){
		$data = ['view' => $view, 'params' => $params];
		$data['obj'] = $this;
		$this->load->view('template/view_ace_base_template', $data);
	}

	public function semesterAktif(){
		$result = $this->db->get_where('ref_tahun_ajaran', array('aktif' => '1'))->row();
		$_result = '';
		if (!empty($result)){
			$_result = $result->tahun_mulai.'/'.$result->tahun_selesai;
		}
		return $_result;
	}

	public function stringSemesterAktif(){
		$result = $this->db->get_where('ref_tahun_ajaran', array('aktif' => '1'))->row();
		$_result = '';
		if (!empty($result) && $result->semester == '1'){
			$_result = 'Gasal';
		}else
		if (!empty($result) && $result->semester == '2'){
			$_result = 'Genap';
		}
		return $_result;
	}

	public function idSemesterAktif(){
		$result = $this->db->get_where('ref_tahun_ajaran', array('aktif' => '1'))->row();
		$_result = 0;
		if (!empty($result)){
			$_result = $result->id_ref_tahun_ajaran;
		}
		return $_result;
	}
	
}
?>