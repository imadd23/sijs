<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends My_controller {

	public function index()
	{
		if (is_null($this->session->userdata('is_logged_in'))){
			$data = [
				'msg' => $this->session->flashdata('msg')
			];
			$this->load->view('login_view', $data);
		}else{
			redirect(site_url('beranda'));
		}		
	}
}
