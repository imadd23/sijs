<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_profil');
	}

	public function index()
	{
		$data = $this->dataConstruct();
		$data['data'] = $this->model_profil->getDataById($this->session->userdata('login_id'));
		$this->render_view('view_profil', $data);
	}

	private function dataConstruct(){
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['post'] = $this->session->flashdata('post');
		return $data;
	}

	public function edit()
	{
		$data = $this->dataConstruct();
		$data['data'] = $this->model_profil->getDataById($this->session->userdata('login_id'));
		$this->render_view('view_edit_profil', $data);
	}

	public function do_update()
	{
		if (isset($_POST['batal'])){
			redirect(site_url('profil'));
		}

		if (empty($_POST['email'])){
			$this->session->set_flashdata('pesan', ['danger', 'Inputan tidak boleh kosong']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('profil/edit'));
		}

		if ($_POST['password'] != $_POST['repeat_password']){
			$this->session->set_flashdata('pesan', ['danger', 'Password tidak konsisten']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('profil/edit'));
		}

		$data = array(
			'email' => $this->input->post('email') 
			);

		if (!empty($_POST['password'])){
			$data['password'] = MD5($this->input->post('password'));
		}

		$update = $this->model_profil->update($data, array(
			'id_logkar' => $this->session->userdata('login_id')
			));

		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('profil'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('profil/edit'));
		}		
	}
}