<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_profil extends CI_Model {

	protected $table_name = 'login';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT A.*, B.role_name, C.Nama, C.Nip 
        	FROM 
        	$this->table_name AS A
            LEFT JOIN
            auth_role AS B
            ON A.role_id = B.role_id
            LEFT JOIN guru AS C
            ON A.id_guru = C.id_guru ";
        return $query;
    }

    public function getDataById($login_id){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE id_logkar = '".$login_id."'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}
}