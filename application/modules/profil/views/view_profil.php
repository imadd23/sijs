<div class="row">
	<div class="col-xs-12">
		<h3 class="header smaller lighter blue">Profil Saya</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>

		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>
		
		<div id="home" class="tab-pane in active">
			<div class="row">
				<div class="col-xs-12 col-sm-3 center">
					<span class="profile-picture">
						<img class="editable img-responsive" alt="Alex's Avatar" id="avatar2" src="<?php echo base_url('assets/ace/images/avatars/profile-pic.jpg');?>" />
					</span>

					<div class="space space-4"></div>

					<!-- <a href="#" class="btn btn-sm btn-block btn-success">
						<i class="ace-icon fa fa-plus-circle bigger-120"></i>
						<span class="bigger-110">Add as a friend</span>
					</a>

					<a href="#" class="btn btn-sm btn-block btn-primary">
						<i class="ace-icon fa fa-envelope-o bigger-110"></i>
						<span class="bigger-110">Send a message</span>
					</a> -->
				</div><!-- /.col -->

				<div class="col-xs-12 col-sm-9">
					<h4 class="blue">
						<span class="middle"><?php echo !empty($data) ? $data['Nama'] : ''; ?></span>

						<a class="label label-purple arrowed-in-right" href="<?php echo site_url('profil/edit');?>">
							<i class="ace-icon fa fa-pencil smaller-80 align-middle"></i>
							Ubah Profil
						</a>
					</h4>

					<div class="profile-user-info">
						<div class="profile-info-row">
							<div class="profile-info-name"> Email </div>

							<div class="profile-info-value">
								<span><?php echo !empty($data) ? $data['email'] : ''; ?></span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> NIP </div>

							<div class="profile-info-value">
								<span><?php echo !empty($data) ? $data['Nip'] : ''; ?></span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> ID Guru </div>

							<div class="profile-info-value">
								<span><?php echo !empty($data) ? $data['id_guru'] : ''; ?></span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> Hak Akses </div>

							<div class="profile-info-value">
								<span><?php echo !empty($data) ? $data['role_name'] : ''; ?></span>
							</div>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div>
	</div>
</div>