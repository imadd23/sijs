<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Page :: SIJS SMKN 1 Kersana</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/bootstrap.min.css');?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/font-awesome/4.5.0/css/font-awesome.min.css');?>" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/fonts.googleapis.com.css');?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace.min.css');?>" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-part2.min.css');?>" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-rtl.min.css');?>" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-ie.min.css');?>" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo base_url('assets/ace/js/html5shiv.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/respond.min.js');?>"></script>
		<![endif]-->
	</head>

	<body class="login-layout blur-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<span class="red">Sistem Informasi Jadwal Sekolah</span>
									<span class="white" id="id-text2">SMKN 1 Kersana</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-user blue"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>

											<?php echo form_open('auth/do_login', []);?>
												<fieldset>
													<?php if (isset($msg[0])){
														?>
														<div class="alert alert-<?php echo $msg[0];?>">
															<?php echo $msg[1];?>
														</div>
														<?php
													}
													?>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" name="email" placeholder="Email" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" name="password" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<!-- <label class="inline">
															<input type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label> -->

														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											<?php echo form_close();?>

										</div><!-- /.widget-main -->

										<div class="toolbar clearfix"></div>

									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<?php echo base_url('assets/ace/js/jquery-2.1.4.min.js');?>"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="<?php echo base_url('assets/ace/js/jquery-1.11.3.min.js');?>"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url('assets/ace/js/jquery.mobile.custom.min.js');?>'>"+"<"+"/script>");
		</script>
	</body>
</html>