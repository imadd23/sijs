<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_controller {

	public $authAction = 'a';

	public function do_login(){
		$wheres = ['email' => $this->input->post('email'), 'password' => MD5($this->input->post('password'))];
		$result = $this->db->select('*')
				->from('guru')
				->join('login', 'guru.id_guru = login.id_guru')
				->where($wheres)
				->get()->row();
		if (!empty($result)){
			$setData = [
				'is_logged_in' => '1', 
				'login_id' => $result->id_logkar, 
				'id_guru' => $result->id_guru,
				'email' => $result->email,
				'role_id' => $result->role_id,
				'nama' => $result->Nama
				];
			$this->session->set_userdata($setData);
			redirect(site_url('beranda'));
		}else{
			$this->session->set_flashdata([
				'msg' => ['danger', 'Username atau password salah']
				]);
			redirect(site_url('welcome'));
		}
	}

	public function do_logout(){
		$this->session->sess_destroy();
		redirect(site_url('welcome'));
	}
}
