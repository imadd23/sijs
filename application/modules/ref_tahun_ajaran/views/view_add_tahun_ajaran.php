<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Tambah Referensi Tahun Pelajaran</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('ref_tahun_ajaran/do_add', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tahun Mulai </label>

				<div class="col-sm-3">
					<select id="tahun_mulai" name="tahun_mulai" class="form-control" />
						<?php
						foreach ($listTahun as $key => $value) {
							if (isset($post) && $post['tahun_mulai'] == $value){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tahun Selesai </label>

				<div class="col-sm-3">
					<select id="tahun_selesai" name="tahun_selesai" class="form-control" />
						<?php
						foreach ($listTahun as $key => $value) {
							if (isset($post) && $post['tahun_selesai'] == $value){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}
							echo '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<!-- <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Aktif </label>

				<div class="col-sm-3">
					<select id="aktif" name="aktif" class="form-control" />
						<?php
						foreach ($listAktif as $key => $value) {
							if (isset($post) && $post['aktif'] == $key){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}
							echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
						}
						?>
					</select>
				</div>
			</div> -->
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Semester </label>

				<div class="col-sm-3">
					<select id="semester" name="semester" class="form-control" />
						<?php
						foreach ($listSemester as $key => $value) {
							if (isset($post) && $post['semester'] == $key){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}
							echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->