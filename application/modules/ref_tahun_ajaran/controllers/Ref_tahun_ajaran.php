<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_tahun_ajaran extends My_controller {

	public $authAction;
	protected $listAktif = ['0' => 'Tidak Aktif', '1' => 'Aktif'];

	public function __construct(){
		parent::__construct();
		$this->load->model('model_ref_tahun_ajaran');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		
		$data['ta'] = $this->model_ref_tahun_ajaran->getData();
		$data['pesan'] = $this->session->flashdata('pesan');
		$this->render_view('view_tahun_ajaran', $data);
	}

	private function dataConstruct(){
		$data['listAktif'] = $this->listAktif;
		$data['listSemester'] = array(
			'1' => 'Gasal',
			'2' => 'Genap'
			);
		$data['listTahun'] = range(2010, 2020);
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_tahun_ajaran', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_tahun_ajaran'));
		}
		$data = [
			'tahun_mulai' => $this->input->post('tahun_mulai'),
			'tahun_selesai' => $this->input->post('tahun_selesai'),
			'semester' => $this->input->post('semester')
		];
		$add = $this->model_ref_tahun_ajaran->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('ref_tahun_ajaran'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_tahun_ajaran/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_ref_tahun_ajaran->getDataById($id);
		}
		$this->render_view('view_edit_tahun_ajaran', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_tahun_ajaran'));
		}
		$data = [
			'tahun_mulai' => $this->input->post('tahun_mulai'),
			'tahun_selesai' => $this->input->post('tahun_selesai'),
			'semester' => $this->input->post('semester')
		];
		$update = $this->model_ref_tahun_ajaran->update($data, ['id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('ref_tahun_ajaran'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_tahun_ajaran/edit/'.$this->input->post('id_ref_tahun_ajaran')));
		}		
	}

	public function aktifkan($id)
	{
		$this->letMeToAccess('edit');

		$this->db->trans_start();

		// non-aktifkan semua
		$data = [
			'aktif' => '0'
		];
		$update = $this->model_ref_tahun_ajaran->update($data, ['id_ref_tahun_ajaran <>' => 'A']);

		$data = [
			'aktif' => '1'
		];
		$_update = $this->model_ref_tahun_ajaran->update($data, ['id_ref_tahun_ajaran' => $id]);

		if ($update && $_update){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}

		if ($update && $_update){
			$this->session->set_flashdata('pesan', ['success', 'Tahun Ajaran sukses diaktifkan']);
			redirect(site_url('ref_tahun_ajaran'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Tahun Ajaran gagal diaktifkan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_tahun_ajaran'));
		}		
	}

	public function nonaktifkan($id)
	{
		$this->letMeToAccess('edit');
		$data = [
			'aktif' => '0'
		];
		$update = $this->model_ref_tahun_ajaran->update($data, ['id_ref_tahun_ajaran' => $id]);

		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Tahun Ajaran sukses dinon-aktifkan']);
			redirect(site_url('ref_tahun_ajaran'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Tahun Ajaran gagal dinon-aktifkan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_tahun_ajaran'));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_ref_tahun_ajaran->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('ref_tahun_ajaran'));
	}
}
