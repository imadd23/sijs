<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Ubah Kelas</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('detail_kelas/do_update', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tahun Ajaran </label>

				<div class="col-sm-3">
					<select id="id_ref_tahun_ajaran" name="id_ref_tahun_ajaran" class="form-control" />
						<?php
						foreach ($listTahunAjaran as $key => $value) {
							if (isset($post) && $post['id_ref_tahun_ajaran'] == $value['id_ref_tahun_ajaran']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_ref_tahun_ajaran'].'" '.$selected.'>'.$value['ta'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Jenjang </label>

				<div class="col-sm-3">
					<select id="id_ref_jenjang" name="id_ref_jenjang" class="form-control" />
						<?php
						foreach ($listJenjang as $key => $value) {
							if (isset($post) && $post['id_ref_jenjang'] == $value['id_ref_jenjang']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_ref_jenjang'].'" '.$selected.'>'.$value['jenjang'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Jurusan </label>

				<div class="col-sm-3">
					<select id="id_ref_jurusan" name="id_ref_jurusan" class="form-control" />
						<?php
						foreach ($listJurusan as $key => $value) {
							if (isset($post) && $post['id_ref_jurusan'] == $value['id_ref_jurusan']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_ref_jurusan'].'" '.$selected.'>'.$value['nama_jurusan'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Kelas</label>

				<div class="col-sm-3">
					<input type="text" name="nama_kelas" class="form-control" value="<?php echo (isset($post)) ? $post['nama_kelas'] : '';?>">
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<input type="hidden" name="id_detail_kelas" value="<?php echo (isset($post)) ? $post['id_detail_kelas'] : 0;?>">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->