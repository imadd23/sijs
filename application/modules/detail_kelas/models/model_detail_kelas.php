<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_detail_kelas extends CI_Model {

	protected $table_name = 'detail_kelas';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT * FROM
        (
        	SELECT `nama_jurusan`, nama_singkat_jurusan, `jenjang`, CONCAT(tahun_mulai, '/', `tahun_selesai`) AS ta, DK.*
			FROM `detail_kelas` AS DK
			LEFT JOIN
			`ref_jenjang` AS RJ 
			ON RJ.`id_ref_jenjang` = DK.`id_ref_jenjang`
			LEFT JOIN
			`ref_tahun_ajaran` AS RTA
			ON `RTA`.`id_ref_tahun_ajaran` = DK.`id_ref_tahun_ajaran`
			LEFT JOIN
			`ref_jurusan` AS RJU
			ON `RJU`.`id_ref_jurusan` = DK.`id_ref_jurusan`  
		) AS temp1 ";
        return $query;
    }

    public function getListData($options = []){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like."
            ORDER BY ".$options['order']." ".$options['mode']."
            LIMIT ".$options['offset'].", ".$options['limit'];

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTotalData($options){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getTotal(){
        $sql = $this->query();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getTahunAjaran()
	{
		$result = $this->db->select("CONCAT(tahun_mulai, '/', tahun_selesai) AS ta, ref_tahun_ajaran.*")->from('ref_tahun_ajaran')->get()->result_array();
		return $result;
	}

	public function getJenjang()
	{
		$result = $this->db->get('ref_jenjang')->result_array();
		return $result;
	}

	public function getJurusan()
	{
		$result = $this->db->get('ref_jurusan')->result_array();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->get_where($this->table_name, ['id_detail_kelas' => $id])->row_array();
		return $result;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('id_detail_kelas', $id)->delete($this->table_name);
		return $delete;
	}
}
