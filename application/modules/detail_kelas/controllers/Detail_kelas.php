<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_kelas extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_detail_kelas');
	}

	public function index()
	{
		$this->letMeToAccess('view');

		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('detail_kelas/get_data_json');
		$this->render_view('view_kelas', $data);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : 'jenjang';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'asc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "jenjang LIKE '%".$requestData['search']['value']."%'",
                "nama_kelas LIKE '%".$requestData['search']['value']."%'",
                "nama_jurusan LIKE '%".$requestData['search']['value']."%'",
                "nama_singkat_jurusan LIKE '%".$requestData['search']['value']."%'"
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_detail_kelas->getListData($options);
        $totalFiltered = $this->model_detail_kelas->getTotalData($options);
        $totalData = $this->model_detail_kelas->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('detail_kelas/edit/'.$value->id_detail_kelas).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('detail_kelas/delete/'.$value->id_detail_kelas).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('detail_kelas/edit/'.$value->id_detail_kelas).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('detail_kelas/delete/'.$value->id_detail_kelas).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listTahunAjaran'] = $this->model_detail_kelas->getTahunAjaran();
		$data['listJenjang'] = $this->model_detail_kelas->getJenjang();
		$data['listJurusan'] = $this->model_detail_kelas->getJurusan();
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_kelas', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('detail_kelas'));
		}
		$data = [
			'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
			'id_ref_jenjang' => $this->input->post('id_ref_jenjang'),
			'id_ref_jurusan' => $this->input->post('id_ref_jurusan'),
			'nama_kelas' => $this->input->post('nama_kelas')
		];
		$add = $this->model_detail_kelas->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('detail_kelas'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('detail_kelas/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_detail_kelas->getDataById($id);
		}
		$this->render_view('view_edit_kelas', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('detail_kelas'));
		}
		$data = [
			'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
			'id_ref_jenjang' => $this->input->post('id_ref_jenjang'),
			'id_ref_jurusan' => $this->input->post('id_ref_jurusan'),
			'nama_kelas' => $this->input->post('nama_kelas')
		];
		$update = $this->model_detail_kelas->update($data, ['id_detail_kelas' => $this->input->post('id_detail_kelas')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('detail_kelas'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('detail_kelas/edit/'.$this->input->post('id_detail_kelas')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_detail_kelas->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('detail_kelas'));
	}
}
