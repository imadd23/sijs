<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_mapel extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_ref_mapel');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		
		$data['ta'] = $this->model_ref_mapel->getData();
		$data['pesan'] = $this->session->flashdata('pesan');
		$this->render_view('view_mapel', $data);
	}

	private function dataConstruct(){
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_mapel', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_mapel'));
		}
		$data = [
			'kode_mapel' => $this->input->post('kode_mapel'),
			'nama_mapel' => $this->input->post('nama_mapel')
		];
		$add = $this->model_ref_mapel->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('ref_mapel'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_mapel/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_ref_mapel->getDataById($id);
		}
		$this->render_view('view_edit_mapel', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_mapel'));
		}
		$data = [
			'kode_mapel' => $this->input->post('kode_mapel'),
			'nama_mapel' => $this->input->post('nama_mapel')
		];
		$update = $this->model_ref_mapel->update($data, ['id_ref_mapel' => $this->input->post('id_ref_mapel')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('ref_mapel'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_mapel/edit/'.$this->input->post('id_ref_mapel')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_ref_mapel->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('ref_mapel'));
	}
}
