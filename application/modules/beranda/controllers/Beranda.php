<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends My_controller {

	public $authAction;

	public function index(){
		$this->load->model('model_beranda');
		$total_jurusan = $this->model_beranda->getTotalJurusan();
		$total_ruang = $this->model_beranda->getTotalRuang();
		$total_mapel = $this->model_beranda->getTotalMapel();
		$total_guru = $this->model_beranda->getTotalGuru();
		$total_kelas = $this->model_beranda->getTotalKelas();
		$total_tahun_ajaran = $this->model_beranda->getTotalTahunAjaran();
		$tahun_ajaran = $this->model_beranda->getTahunAjaran();
		$semester = $this->model_beranda->getSemester();

		$data = array(
				'total_jurusan' => $total_jurusan,
				'total_ruang' => $total_ruang,
				'total_mapel' => $total_mapel,
				'total_guru' => $total_guru,
				'total_kelas' => $total_kelas,
				'total_tahun_ajaran' => $total_tahun_ajaran,
				'tahun_ajaran' => $tahun_ajaran,
				'semester' => $semester
			);
		$this->render_view('view_beranda', $data);
	}
}