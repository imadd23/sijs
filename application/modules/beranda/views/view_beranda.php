<div class="row">
	<div class="col-xs-12">
		<blockquote>
			<p class="lighter line-height-125">
				Selamat Datang di Sistem Informasi Jadwal Sekolah SMKN 1 Kersana
			</p>

			<small>
				Let's do more
				<cite title="Source Title">with technology</cite>
			</small>
		</blockquote>
	</div>
	<div class="col-xs-12">
		<div class="col-sm-7 infobox-container">
			<div class="infobox infobox-green">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-cog"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_jurusan;?></span>
					<div class="infobox-content">Total Jurusan</div>
				</div>

				<!-- <div class="stat stat-success">8%</div> -->
			</div>

			<div class="infobox infobox-blue">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-cog"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_ruang;?></span>
					<div class="infobox-content">Total Ruang</div>
				</div>

				<!-- <div class="badge badge-success">
					+32%
					<i class="ace-icon fa fa-arrow-up"></i>
				</div> -->
			</div>

			<div class="infobox infobox-pink">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-book"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_mapel;?></span>
					<div class="infobox-content">Total Mapel</div>
				</div>
				<!-- <div class="stat stat-important">4%</div> -->
			</div>

			<div class="infobox infobox-red">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-user"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_guru;?></span>
					<div class="infobox-content">Total Guru</div>
				</div>
			</div>

			<div class="infobox infobox-orange2">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-home"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_kelas;?></span>
					<div class="infobox-content">Total Kelas</div>
				</div>
			</div>

			<div class="infobox infobox-blue2">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-calendar"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $total_tahun_ajaran;?></span>
					<div class="infobox-content">Total Tahun Ajaran</div>
				</div>
			</div>

			<div class="infobox infobox-purple">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-calendar"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $tahun_ajaran;?></span>
					<div class="infobox-content">Tahun Ajaran Aktif</div>
				</div>
			</div>
			<div class="infobox infobox-pink">
				<div class="infobox-icon">
					<i class="ace-icon fa fa-calendar"></i>
				</div>

				<div class="infobox-data">
					<span class="infobox-data-number"><?php echo $semester;?></span>
					<div class="infobox-content">Semester Aktif</div>
				</div>
			</div>
			<div class="infobox"></div>
			<div class="space-6"></div>

			
		</div>
	</div>
</div>