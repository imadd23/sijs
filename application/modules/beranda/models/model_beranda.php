<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_beranda extends CI_Model {

	protected $table_name = '';

	public function __construct(){
		parent::__construct();
	}

	public function getTotalJurusan(){
		return $this->db->get('ref_jurusan')->num_rows();
	}

	public function getTotalRuang(){
		return $this->db->get('ref_ruang')->num_rows();
	}

	public function getTotalMapel(){
		return $this->db->get('ref_mapel')->num_rows();
	}

	public function getTotalGuru(){
		return $this->db->get('guru')->num_rows();
	}

	public function getTotalKelas(){
		return $this->db->get('detail_kelas')->num_rows();
	}

	public function getTotalTahunAjaran(){
		return $this->db->get('ref_tahun_ajaran')->num_rows();
	}

	public function getTahunAjaran(){
		$result = $this->db->select("CONCAT(tahun_mulai, '/', tahun_selesai) AS tahun")->from('ref_tahun_ajaran')->where(array('aktif' => '1'))->get()->row();
		if (empty($result)){
			return 'Belum diset';
		}else{
			return $result->tahun;
		}
	}

	public function getSemester(){
		$result = $this->db->select("IF(semester = '1', 'Gasal', IF(semester = '2', 'Genap', '')) AS semester")->from('ref_tahun_ajaran')->where(array('aktif' => '1'))->get()->row();
		if (empty($result)){
			return 'Belum diset';
		}else{
			return $result->semester;
		}
	}
}