<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_jurusan extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_ref_jurusan');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		
		$data['ta'] = $this->model_ref_jurusan->getData();
		$data['pesan'] = $this->session->flashdata('pesan');
		$this->render_view('view_jurusan', $data);
	}

	private function dataConstruct(){
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_jurusan', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_jurusan'));
		}
		$data = [
			'nama_jurusan' => $this->input->post('nama_jurusan'),
			'nama_singkat_jurusan' => $this->input->post('nama_singkat_jurusan')
		];
		$add = $this->model_ref_jurusan->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('ref_jurusan'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_jurusan/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_ref_jurusan->getDataById($id);
		}
		$this->render_view('view_edit_jurusan', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_jurusan'));
		}
		$data = [
			'nama_jurusan' => $this->input->post('nama_jurusan'),
			'nama_singkat_jurusan' => $this->input->post('nama_singkat_jurusan')
		];
		$update = $this->model_ref_jurusan->update($data, ['id_ref_jurusan' => $this->input->post('id_ref_jurusan')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('ref_jurusan'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_jurusan/edit/'.$this->input->post('id_ref_jurusan')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_ref_jurusan->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('ref_jurusan'));
	}
}
