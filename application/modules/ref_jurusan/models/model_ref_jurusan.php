<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ref_jurusan extends CI_Model {

	protected $table_name = 'ref_jurusan';

	public function __construct(){
		parent::__construct();
	}

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->get_where($this->table_name, ['id_ref_jurusan' => $id])->row_array();
		return $result;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('id_ref_jurusan', $id)->delete($this->table_name);
		return $delete;
	}
}
