<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru_mapel extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_guru_mapel');
	}

	public function index()
	{
		$this->letMeToAccess('view');

		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('guru_mapel/get_data_json');
		$this->render_view('view_guru_mapel', $data);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : 'jenjang';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'asc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "ta LIKE '%".$requestData['search']['value']."%'",
                "Nama LIKE '%".$requestData['search']['value']."%'",
                "Nip LIKE '%".$requestData['search']['value']."%'",
                "kode_mapel LIKE '%".$requestData['search']['value']."%'"
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_guru_mapel->getListData($options);
        $totalFiltered = $this->model_guru_mapel->getTotalData($options);
        $totalData = $this->model_guru_mapel->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('guru_mapel/edit/'.$value->id_guru_mapel).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('guru_mapel/delete/'.$value->id_guru_mapel).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('guru_mapel/edit/'.$value->id_guru_mapel).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('guru_mapel/delete/'.$value->id_guru_mapel).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
				$value->warna = '<button class="btn btn-sm" style="background-color:'.(!empty($value->warna) ? $value->warna : 'orange').' !important;">'.$value->id_guru.'</buton>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listTahunAjaran'] = $this->model_guru_mapel->getTahunAjaran();
		$data['listGuru'] = $this->model_guru_mapel->getGuru();
		$data['listMapel'] = $this->model_guru_mapel->getMapel();
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_guru_mapel', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('guru_mapel'));
		}
		$data = [
			'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
			'id_ref_mapel' => $this->input->post('id_ref_mapel'),
			'id_guru' => $this->input->post('id_guru'),
			'warna' => $this->input->post('warna')
		];
		$add = $this->model_guru_mapel->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('guru_mapel'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('guru_mapel/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_guru_mapel->getDataById($id);
		}
		$this->render_view('view_edit_guru_mapel', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('guru_mapel'));
		}
		$data = [
			'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
			'id_ref_mapel' => $this->input->post('id_ref_mapel'),
			'id_guru' => $this->input->post('id_guru'),
			'warna' => $this->input->post('warna')
		];
		$update = $this->model_guru_mapel->update($data, ['id_guru_mapel' => $this->input->post('id_guru_mapel')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('guru_mapel'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('guru_mapel/edit/'.$this->input->post('id_guru_mapel')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_guru_mapel->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('guru_mapel'));
	}
}
