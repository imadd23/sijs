<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_guru_mapel extends CI_Model {

	protected $table_name = 'guru_mapel';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT * FROM
        (
        	SELECT `kode_mapel`, nama_mapel, G.Nama, G.Nip, CONCAT(tahun_mulai, '/', `tahun_selesai`) AS ta, GM.*
			FROM $this->table_name AS GM
			LEFT JOIN
			`ref_tahun_ajaran` AS RTA
			ON `RTA`.`id_ref_tahun_ajaran` = GM.`id_ref_tahun_ajaran`
			LEFT JOIN
			`guru` AS G
			ON `G`.`id_guru` = GM.`id_guru`
			LEFT JOIN
			`ref_mapel` AS M
			ON M.`id_ref_mapel` = GM.`id_ref_mapel` 
		) AS temp1 ";
        return $query;
    }

    public function getListData($options = []){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like."
            ORDER BY ".$options['order']." ".$options['mode']."
            LIMIT ".$options['offset'].", ".$options['limit'];

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTotalData($options){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getTotal(){
        $sql = $this->query();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getTahunAjaran()
	{
		$result = $this->db->select("CONCAT(tahun_mulai, '/', tahun_selesai) AS ta, ref_tahun_ajaran.*")->from('ref_tahun_ajaran')->get()->result_array();
		return $result;
	}

	public function getMapel()
	{
		$result = $this->db->get('ref_mapel')->result_array();
		return $result;
	}

	public function getGuru()
	{
		$result = $this->db->get('guru')->result_array();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->get_where($this->table_name, ['id_guru_mapel' => $id])->row_array();
		return $result;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('id_guru_mapel', $id)->delete($this->table_name);
		return $delete;
	}
}
