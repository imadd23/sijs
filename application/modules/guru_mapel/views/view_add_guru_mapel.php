<link href="<?php echo base_url('assets/ace/plugin/colorpicker/css/evol-colorpicker.min.css');?>" rel="stylesheet" />
<style type="text/css">
.evo-pop{
	background-color: white;
}
</style>
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Tambah Guru Mapel</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('guru_mapel/do_add', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tahun Ajaran </label>

				<div class="col-sm-3">
					<select id="id_ref_tahun_ajaran" name="id_ref_tahun_ajaran" class="form-control" />
						<?php
						foreach ($listTahunAjaran as $key => $value) {
							if (isset($post) && $post['id_ref_tahun_ajaran'] == $value['id_ref_tahun_ajaran']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_ref_tahun_ajaran'].'" '.$selected.'>'.$value['ta'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Guru </label>

				<div class="col-sm-4">
					<select id="id_guru" name="id_guru" class="form-control" />
						<?php
						foreach ($listGuru as $key => $value) {
							if (isset($post) && $post['id_guru'] == $value['id_guru']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_guru'].'" '.$selected.'>'.$value['Nip'].' | '.$value['Nama'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Mapel </label>

				<div class="col-sm-4">
					<select id="id_ref_mapel" name="id_ref_mapel" class="form-control"/>
						<?php
						foreach ($listMapel as $key => $value) {
							if (isset($post) && $post['id_ref_mapel'] == $value['id_ref_mapel']){
								$selected = 'selected="selected"';
							}else
							if ($value['aktif'] == '1'){
								$selected = 'selected="selected"';
							}else{
								$selected = "";
							}

							echo '<option value="'.$value['id_ref_mapel'].'" '.$selected.'>'.$value['nama_mapel'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Warna </label>

				<div class="col-sm-4">
					<input id="cpButton" name="warna" value="#92cddc" readonly/>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
<!-- colorpicker -->
<script src="<?php echo base_url('assets/ace/plugin/colorpicker/js/evol-colorpicker.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#cpButton').colorpicker({showOn:'button'});
	});
</script>