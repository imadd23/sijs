<style type="text/css">
	/*.btn-container{
		padding: 3px; 
		float: left;
	}*/
	.dropped-area{
		color: white;
		cursor: pointer;
	}
	.dropped-area-ruang{
		background-color: #FFB752;
		color: white;
		cursor: pointer;
		max-width: 30px;
		overflow-x: hidden;
		font-size: 9px;
		white-space: nowrap;
	}
	/*.label-drag-area{
		display: inline-block;
		position: absolute;
		line-height: initial;
	}*/
	.dd-dragel > li > .dd2-handle, .dd2-handle {
		overflow: visible;
		margin: 0px;
		line-height: initial;
	}
	.dropped-area{
		color: white;
		cursor: pointer;
	}
	.drag-selector{
		width: 51px;
		min-height: 40px;
	    max-height: 40px;
	}
	.dd2-handle + .dd2-content, .dd2-handle + .dd2-content[class*="btn-"] {
	    padding-left: 55px;
	    min-height: 42px;
	    max-height: 42px;
	    white-space: nowrap;
	    overflow-x: hidden; 
	}

	th.width-32, td.width-32 {
		width: 50px !important;
		min-width: 50px !important;
		max-width: 50px !important;
	}

	thead, tbody { display: block; }

	tbody {
	    height: 350px;       /* Just for the demo          */
	    overflow-y: auto;    /* Trigger vertical scroll    */
	    overflow-x: hidden;  /* Hide the horizontal scroll */
	}
</style>
<div class="row">
	<div class="col-xs-12">
		<h3 class="row header smaller lighter blue">
			<span class="col-sm-6"> Kelola Ruang Pelajaran <?php echo $semester;?></span><!-- /.col -->

			<span class="col-sm-6">
				<label class="pull-right inline">
					<a class="btn btn-xs btn-primary" href="<?php echo site_url('jadwal_pelajaran');?>">
						<i class="ace-icon fa fa-arrow-left bigger-110"></i>
						Kembali
					</a>
				</label>
			</span><!-- /.col -->
		</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>

		<div class="row">
			<div class="col-sm-9">
				<div class="table-responsive">
					<table class="table table-bordered" id="table-jadwal">
						<?php
						$aDataJam = [];
						if (!empty($listAlokasiJam)){
							$headerKelas = '';
							echo '<thead><tr><th colspan="4" class="text-center nama_kelas" style="vertical-align:middle; width:200px;">NAMA</th>';
							echo $headerKelas;
							foreach ($listAlokasiJam as $key => $value) {
								echo '<th class="text-center" colspan="'.$value['jumlah_data'].'">'.$value['hari'].'</th>';
							}
							echo '</tr><tr><th colspan="4" class="text-center nama_kelas" style="vertical-align:middle; width:200px;">KELAS</th>';
							foreach ($listAlokasiJam as $key => $value) {
								$_jam = str_replace("#", '"', $value['jam']);
								$jam = json_decode($_jam, true);
								if (!empty($jam)){
									foreach ($jam as $key => $value) {
										$aDataJam[] = $value['id'];
										echo '<th class="text-center width-32"><div>'.$value['kode_jam'].'</div></th>';
									}
								}
							}
							echo '</tr></thead>';
						}

						if (!empty($listDetailKelas)){
							echo '<tbody>';
							foreach ($listDetailKelas as $key => $value) {
								$_aJenjang = str_replace('#', '"', $value['jenjang']);
								$aJenjang = json_decode($_aJenjang, true);
								$rowspanJurusan = $value['jumlah_data'] * 2;
								echo "<tr>";
								echo "<th class='text-center width-32' style='writing-mode: vertical-rl; vertical-align: middle;' rowspan='".$rowspanJurusan."'>".$value['nama_jurusan'].'</th>';

								if (!empty($aJenjang)){
									foreach ($aJenjang as $k => $v) {
										$aId = $this->model_jadwal->getJenjangById($value['id_ref_jurusan'], $v['id_jenjang'], $value['id_ref_tahun_ajaran']);
										$rowspanJenjang = $v['jumlah_data'] * 2;
										echo '<th class="text-center width-32" style="writing-mode: vertical-rl; vertical-align: middle;" rowspan="'.$rowspanJenjang.'">'.$v['jenjang'].'</th>';
										if (!empty($aId)){
											foreach ($aId as $kk => $vv) {
												$dKelas = $this->model_jadwal->getDetailKelasById($vv['id_detail_kelas']);
												if ($kk == 0 && !empty($dKelas)){
													echo '<th class="text-center width-32" rowspan="2" style="writing-mode: vertical-rl; vertical-align: middle;">'.$dKelas['nama_singkat_jurusan'].'</th>
														<th class="text-center width-32" rowspan="2" style="vertical-align: middle;">'.$dKelas['nama_kelas'].'</th>';
													if (!empty($aDataJam)){
														foreach ($aDataJam as $kkk => $vvv) {

															echo '<td class="text-center drop-area width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
														}
													}
													echo "</tr>";
												}else
												if (!empty($dKelas)){
													echo '<tr>
														<th class="text-center width-32" rowspan="2" style="writing-mode: vertical-rl; vertical-align: middle;">'.$dKelas['nama_singkat_jurusan'].'</th>
														<th class="text-center width-32" rowspan="2" style="vertical-align: middle;">'.$dKelas['nama_kelas'].'</th>';
													if (!empty($aDataJam)){
														foreach ($aDataJam as $kkk => $vvv) {

															echo '<td class="text-center drop-area width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
														}
													}
													echo "</tr>";
												}

												// make space for ruang kelas
												echo '<tr>';
												if (!empty($aDataJam)){
													foreach ($aDataJam as $kkk => $vvv) {

														echo '<td class="text-center drop-area-ruang width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
													}
												}
												echo "</tr>";										
											}
										}
										
									}
								}
							}
							echo '</tbody>';
						}
						?>
					</table>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="widget-box" style="max-height:500px; overflow-y: scroll;">
					<div class="widget-header">
						<h4>Ruang</h4>
					</div>

					<div class="widget-body">
						<div class="widget-main no-padding">
							<div class="dd dd-draghandle">
								<ol class="dd-list">
								<?php if (!empty($listRuang)){
									foreach ($listRuang as $key => $value) {
										?>
										<li class="dd-item dd2-item">
											<div class="dd-handle dd2-handle">
												<a class="btn btn-warning drag-selector" 
												data-class="label-yellow" 
												data-id-ref-ruang="<?php echo $value['id_ref_ruang'];?>" 
												data-nama-ruang="<?php echo $value['nama_ruang'];?>" 
												data-nama-kelas="<?php echo $value['nama_kelas'];?>"
												>
													<?php echo $value['nama_ruang'];?>
												</a>
											</div>
											<div class="dd2-content">
												<?php echo $value['nama_kelas'];?>
											</div>
										</li>
										<?php
									}
								}?>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<!-- confirm delete -->
<div id="dialog-confirm" class="hide">
	<div class="space-6"></div>

	<p class="bigger-110 bolder center grey">
		<i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
		Apakah Anda yakin akan menghapus?
	</p>
</div><!-- #dialog-confirm -->

<div id="dialog-confirm-replace" title="Konfirmasi">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><div id="content-confirm">Jam ini sudah terisi</div></p>
</div>

<!-- notification dialog -->
<div id="dialog-confirm-notification" title="Informasi">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><div id="content-confirm-notification"></div></p>
</div>
<!-- ./notification dialog -->
<script type="text/javascript">
	var loader = '<img src="<?php echo base_url('assets/ace/images/loader/ajax-loader.gif');?>">';

	$(document).ready(function(){
		var td = $('td.drop-area');
		var total = td.length;
		var limit = 100;
		var childs = new Array();
		var params = new Array();
		var i = 0;
		var j = 0;
		var td_ruang = $('td.drop-area-ruang');
		var total_ruang = $('td.drop-area-ruang').length;
		var childs_ruang = new Array();
		var params_ruang = new Array();
		// console.log((total_ruang));
		$.each(td, function(index, value){
			// pre condition
			var id_detail_kelas = $(this).attr('data-id-detail-kelas');
			var id_alokasi_jam = $(this).attr('data-id-alokasi-jam');

			if (j == limit || (index + 1) == total){
				// send to background;
				params.push(childs);
				$.ajax({
					url : '<?php echo site_url('jadwal_pelajaran/get_child_json');?>',
					type : 'post',
					dataType : 'jSON',
					data : {
						ids : childs
					},
					success : function(response){
						if (typeof (response) && response.success){
							$.each(response.data, function(i, v){
								$('td.drop-area:eq('+v.index+')').addClass(v.class).css('background-color', v.warna).html(v.kode_guru);
							});
						}
						// $('th.nama_kelas').css({'width' : '150px'});
					},
					error : function(response){

					}
				});

				childs = [];
				j = 0;
			}

			childs[j] = {
				'index' : index,
				'id_detail_kelas' : id_detail_kelas,
				'id_alokasi_jam' : id_alokasi_jam
			};
			j++;
		});

		$.each(td_ruang, function(index, value){
			// pre condition
			var id_detail_kelas = $(this).attr('data-id-detail-kelas');
			var id_alokasi_jam = $(this).attr('data-id-alokasi-jam');

			if (i == limit || (index + 1) == total_ruang){
				// send to background;
				// params_ruang.push(childs_ruang);
				$.ajax({
					url : '<?php echo site_url('jadwal_pelajaran/get_child_ruang_json');?>',
					type : 'post',
					dataType : 'jSON',
					data : {
						ids : childs_ruang
					},
					success : function(response){
						if (typeof (response) && response.success){
							$.each(response.data, function(i, v){
								$('td.drop-area-ruang:eq('+v.index+')').addClass(v.class).html(v.nama_ruang);
							});
						}
						// $('th.nama_kelas').css({'width' : '150px'});
					},
					error : function(response){

					}
				});

				childs_ruang = [];
				i = 0;
			}

			childs_ruang[i] = {
				'index' : index,
				'id_detail_kelas' : id_detail_kelas,
				'id_alokasi_jam' : id_alokasi_jam
			};
			i++;
		});
		// console.log(total);
		// console.log(params);
	});

	jQuery(function($) {
		// $('th.nama_kelas').css({'width' : '150px'});
		$("#dialog-confirm").dialog().dialog('close');
		$('#dialog-confirm-replace').dialog().dialog('close');
		$('#dialog-confirm-notification').dialog().dialog('close');
		
		$('.drag-selector').draggable({
			zIndex: 999,
			revert: true,      // will cause the event to go back to its
			revertDuration: 0,  //  original position after the drag
			appendTo: 'body',
			helper: 'clone',
			drag: function( event, ui ) {
			},
			start: function(event, ui){
			},
			stop: function( event, ui ) {
			}
		});	

		$( ".drop-area-ruang" ).droppable({
	      	drop: function( event, ui ) {
	      		var selector = $(this);
	      		var warna = ui.draggable.attr('data-warna');
	      		var content = $(this).html();
	      		var forceSave = false;
	      		selector.removeClass('bg-primary').html(loader);
				if (content != ''){
					$("#dialog-confirm-replace").find('#content-confirm').html('Ruang ini sudah terisi oleh '+content);
					$( "#dialog-confirm-replace" ).dialog({
						resizable: false,
						height: "auto",
						width: 400,
						modal: true,
						buttons: {
							// "Replace": function() {
							// 	$( this ).dialog( "close" );
							//   	$.ajax({
					  //         		url : '<?php echo site_url('jadwal_pelajaran/do_add_ruang');?>',
					  //         		type : 'post',
					  //         		dataType : 'jSON',
					  //         		data : {
					  //         			id_alokasi_jam : selector.attr('data-id-alokasi-jam'),
					  //         			id_detail_kelas : selector.attr('data-id-detail-kelas'),
					  //         			id_ref_ruang : ui.draggable.attr('data-id-ref-ruang')
					  //         		},
					  //         		success : function(response){
					  //         			if (response.success == '1'){
					  //         				selector
							// 	        	.addClass('dropped-area-ruang')
							// 	          	// .html(ui.draggable.html());
							// 	          	.html(response.data);
					  //         			}else{
					  //         				$("#dialog-confirm-notification").find('#content-confirm-notification').html(response.message);
					  //         				$( "#dialog-confirm-notification" ).dialog({
							// 					resizable: false,
							// 					height: "auto",
							// 					width: 400,
							// 					modal: true,
							// 					buttons: {
							// 						"Keluar" : function(){
							// 							$( this ).dialog( "close" );
							// 						}
							// 					}
							// 				});
					  //         				selector.html('');
					  //         			}
					  //         		},
					  //         		error : function(response){
					  //         			selector.html('');
					  //         		}
					  //         	});
							// },
							"Keluar": function() {
							  	$( this ).dialog( "close" );
								selector.html(content);
								// selector.removeClass('dropped-area-ruang').html('');
							}
						}
					});
				}else{
					forceSave = true;
				}
	      		
	      		if (forceSave){
	      			$.ajax({
		          		url : '<?php echo site_url('jadwal_pelajaran/do_add_ruang');?>',
		          		type : 'post',
		          		dataType : 'jSON',
		          		data : {
		          			id_alokasi_jam : $(this).attr('data-id-alokasi-jam'),
		          			id_detail_kelas : $(this).attr('data-id-detail-kelas'),
		          			id_ref_ruang : ui.draggable.attr('data-id-ref-ruang')
		          		},
		          		success : function(response){
		          			if (response.success == '1'){
		          				selector
					        	.addClass('dropped-area-ruang')
					          	// .html(ui.draggable.html());
					          	.html(response.data);
		          			}else{
		          				$("#dialog-confirm-notification").find('#content-confirm-notification').html(response.message);
		          				$( "#dialog-confirm-notification" ).dialog({
									resizable: false,
									height: "auto",
									width: 400,
									modal: true,
									buttons: {
										"Keluar" : function(){
											$( this ).dialog( "close" );
										}
									}
								});
		          				selector.html('');
		          			}
		          		},
		          		error : function(response){
		          			selector.html('');
		          		}
		          	});
	      		}	          	
	      	},
	      	out: function( event, ui ) {
	      		$(this).removeClass('bg-primary');
	      	},
	      	over: function( event, ui ) {
	      		$(this).addClass('bg-primary');
	      	}
	    });

		$(document).on('click', '.dropped-area-ruang', function(e){
			e.preventDefault();
			e.stopPropagation();
			if ($(this).html() == ''){
				return false;
			}

			var _href = '<?php echo site_url("jadwal_pelajaran/delete_ruang")?>';
			var selector = $(this);
			var preventHtml = $(this).html();
			$( "#dialog-confirm" ).removeClass('hide').dialog({
				resizable: false,
				width: '320',
				modal: true,
				height : "auto",
				title: "Konfirmasi Hapus",
				// title_html: false,
				buttons: {
					"Hapus" : function() {
						$( this ).dialog( "close" );
			      		selector.removeClass('bg-primary').html(loader);
			          	$.ajax({
			          		url : '<?php echo site_url('jadwal_pelajaran/do_delete_ruang');?>',
			          		type : 'post',
			          		dataType : 'jSON',
			          		data : {
			          			id_alokasi_jam : selector.attr('data-id-alokasi-jam'),
			          			id_detail_kelas : selector.attr('data-id-detail-kelas')
			          		},
			          		success : function(response){
			          			if (response.success){
			          				// selector.removeClass('dropped-area-ruang').css('background-color', '#FFFFFF').html('');
			          				selector.removeClass('dropped-area-ruang').html('');
			          			}else{
			          				selector
						        	.addClass('dropped-area-ruang')
						          	.html(preventHtml);
			          			}
			          		},
			          		error : function(response){
			          			selector
					        	.addClass('dropped-area-ruang')
					          	.html(preventHtml);
			          		}
			          	});
					},
					"Batal" : function() {
						$( this ).dialog( "close" );
					}
				}
			});	
		});
	});
</script>