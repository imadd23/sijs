<style type="text/css">
	.btn-container{
		padding: 3px; 
		float: left;
	}
	.dropped-area{
		color: white;
		cursor: pointer;
	}
	.dropped-area-ruang{
		background-color: #FFB752;
		color: white;
		cursor: pointer;
		font-size: 9px;
		white-space: nowrap;
		max-width: 30px;
		overflow-x: hidden;
	}
	.label-drag-area{
		display: inline-block;
		position: absolute;
		line-height: initial;
	}

	th.width-32, td.width-32 {
		width: 50px !important;
		min-width: 50px !important;
		max-width: 50px !important;
	}

	thead, tbody { display: block; }

	tbody {
	    height: 350px;       /* Just for the demo          */
	    overflow-y: auto;    /* Trigger vertical scroll    */
	    overflow-x: hidden;  /* Hide the horizontal scroll */
	}

	th.freezedCol, td.freezedCol{
		/*position:absolute; */
        /*width:5em; */
        /*left:0;
        top:auto;
        overflow-x: visible; */
        /*border-right: 0px none black; */
        /*border-top-width:3px;*/ /*only relevant for first row*/
        /*margin-top:-3px;*/ /*compensate for top border*/
	}
</style>
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/ace/plugin/perfect-scrollbar/css/perfect-scrollbar.css');?>">-->
<div class="row">
	<div class="col-xs-12">
		<h3 class="row header smaller lighter blue">
			<span class="col-sm-6"> Jadwal Pelajaran <?php echo $semester;?></span><!-- /.col -->
			
			<span class="col-sm-6">
				<label class="pull-right inline">
					<?php if ($obj->has_access('jadwal_pelajaran', 'view')){ ?>
						<a class="btn btn-xs btn-primary" target="_blank" href="<?php echo site_url('jadwal_pelajaran/export');?>">
							<i class="ace-icon fa fa-file-excel-o bigger-110"></i>
							Export
						</a>
					<?php } ?>

					<?php if ($obj->has_access('jadwal_pelajaran', 'add')){ ?>
						<a class="btn btn-xs btn-primary" href="<?php echo site_url('jadwal_pelajaran/edit');?>">
							<i class="ace-icon fa fa-pencil bigger-110"></i>
							Kelola Jadwal
						</a>
						<a class="btn btn-xs btn-primary" href="<?php echo site_url('jadwal_pelajaran/edit_ruang');?>">
							<i class="ace-icon fa fa-pencil bigger-110"></i>
							Kelola Ruang
						</a>
					<?php }?>
				</label>
			</span><!-- /.col -->
			
		</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>

		<div class="table-responsive scrollable-horizontal scrollable-vertical" data-size="800">
			<table class="table table-bordered" id="table-jadwal">
				<?php
				$aDataJam = [];
				if (!empty($listAlokasiJam)){
					// $headerKelas = '<th colspan="4" rowspan="2" class="text-center nama_kelas" style="vertical-align:middle; width: 128px;">NAMA KELAS</th>';
					echo '<thead><tr><th colspan="4" class="text-center nama_kelas freezedCol" style="width: 200px !important;">NAMA</th>';
					// echo $headerKelas;
					foreach ($listAlokasiJam as $key => $value) {
						echo '<th class="text-center width-32" colspan="'.$value['jumlah_data'].'">'.$value['hari'].'</th>';
					}
					echo '</tr><tr><th colspan="4" class="text-center nama_kelas freezedCol" style="width: 200px !important;">KELAS</th>';
					foreach ($listAlokasiJam as $key => $value) {
						$_jam = str_replace("#", '"', $value['jam']);
						$jam = json_decode($_jam, true);
						if (!empty($jam)){
							foreach ($jam as $key => $value) {
								$aDataJam[] = $value['id'];
								echo '<th class="text-center width-32"><div style="width:32px">'.$value['kode_jam'].'</div></th>';
							}
						}
					}
					echo '</tr></thead>';
				}

				if (!empty($listDetailKelas)){
					echo '<tbody>';
					foreach ($listDetailKelas as $key => $value) {
						$_aJenjang = str_replace('#', '"', $value['jenjang']);
						$aJenjang = json_decode($_aJenjang, true);
						$rowspanJurusan = $value['jumlah_data'] * 2;
						echo "<tr>";
						echo "<th class='text-center width-32 freezedCol' style='writing-mode: vertical-rl; vertical-align: middle;' rowspan='".$rowspanJurusan."' nowrap>".$value['nama_jurusan'].'</th>';

						if (!empty($aJenjang)){
							foreach ($aJenjang as $k => $v) {
								$aId = $this->model_jadwal->getJenjangById($value['id_ref_jurusan'], $v['id_jenjang'], $value['id_ref_tahun_ajaran']);
								$rowspanJenjang = $v['jumlah_data'] * 2;
								echo '<th class="text-center width-32 freezedCol" style="writing-mode: vertical-rl; vertical-align: middle;" rowspan="'.$rowspanJenjang.'" nowrap>'.$v['jenjang'].'</th>';
								if (!empty($aId)){
									foreach ($aId as $kk => $vv) {
										$dKelas = $this->model_jadwal->getDetailKelasById($vv['id_detail_kelas']);
										if ($kk == 0 && !empty($dKelas)){
											echo '<th class="text-center width-32 freezedCol" style="writing-mode: vertical-rl; vertical-align: middle;" rowspan="2" nowrap>'.$dKelas['nama_singkat_jurusan'].'</th>
												<th class="text-center width-32 freezedCol" style="vertical-align: middle;" rowspan="2" nowrap>'.$dKelas['nama_kelas'].'</th>';
											if (!empty($aDataJam)){
												foreach ($aDataJam as $kkk => $vvv) {
													echo '<td class="text-center drop-area width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
												}
											}
											echo "</tr>";
										}else
										if (!empty($dKelas)){
											echo '<tr>
												<th class="text-center width-32 freezedCol" style="writing-mode: vertical-rl; vertical-align: middle;" rowspan="2" nowrap>'.$dKelas['nama_singkat_jurusan'].'</th>
												<th class="text-center width-32 freezedCol" style="vertical-align: middle;" rowspan="2" nowrap>'.$dKelas['nama_kelas'].'</th>';
											if (!empty($aDataJam)){
												foreach ($aDataJam as $kkk => $vvv) {
													echo '<td class="text-center drop-area width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
												}
											}
											echo "</tr>";
										}		

										// make space for ruang kelas
										echo '<tr>';
										if (!empty($aDataJam)){
											foreach ($aDataJam as $kkk => $vvv) {

												echo '<td class="text-center drop-area-ruang width-32" data-id-detail-kelas="'.$vv['id_detail_kelas'].'" data-id-alokasi-jam="'.$vvv.'"></td>';
											}
										}
										echo "</tr>";	

									}
								}
								
							}
						}
					}
					echo '</tbody>';
				}
				?>
			</table>
		</div>
		
	</div>
</div>

<!-- confirm delete -->
<div id="dialog-confirm" class="hide">
	<div class="space-6"></div>

	<p class="bigger-110 bolder center grey">
		<i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
		Apakah Anda yakin akan menghapus kelas?
	</p>
</div><!-- #dialog-confirm -->
<!--<script type="text/javascript" src="<?php echo base_url('assets/ace/plugin/perfect-scrollbar/js/perfect-scrollbar.jquery.js');?>"></script>-->
<script type="text/javascript">
	var widthT = $('#table-jadwal').closest('div').width();

	jQuery(function($) {
	});

	function makeTable(){
	}	

	$('#table-jadwal').ready(function(){

		var td = $('td.drop-area');
		var total = td.length;
		var limit = 100;
		var childs = new Array();
		var params = new Array();
		var i = 0;
		var j = 0;
		var td_ruang = $('td.drop-area-ruang');
		var total_ruang = $('td.drop-area-ruang').length;
		var childs_ruang = new Array();
		var params_ruang = new Array();

		$.each(td, function(index, value){
			// pre condition
			var id_detail_kelas = $(this).attr('data-id-detail-kelas');
			var id_alokasi_jam = $(this).attr('data-id-alokasi-jam');

			if (j == limit || (index + 1) == total){
				// send to background;
				params.push(childs);
				$.ajax({
					url : '<?php echo site_url('jadwal_pelajaran/get_child_json');?>',
					type : 'post',
					dataType : 'jSON',
					data : {
						ids : childs
					},
					success : function(response){
						if (typeof (response) && response.success){
							$.each(response.data, function(i, v){
								var contentHtml = '<div href="#" class="popover-info" data-rel="popover" data-placement="bottom" title="Detail" data-content="'+v.detail+'">'+v.kode_guru+'</div>';
								$('td.drop-area:eq('+v.index+')').addClass(v.class).css('background-color', v.warna).html(contentHtml);
							});
						}
						// $('th.nama_kelas').css({'width' : '150px'});
						$('[data-rel=popover]').popover({html:true});

						// if (index + 1 == total){
						// 	makeTable();
						// }
					},
					error : function(response){

					}
				});

				childs = [];
				j = 0;
			}

			childs[j] = {
				'index' : index,
				'id_detail_kelas' : id_detail_kelas,
				'id_alokasi_jam' : id_alokasi_jam
			};
			j++;
		});

		$.each(td_ruang, function(index, value){
			// pre condition
			var id_detail_kelas = $(this).attr('data-id-detail-kelas');
			var id_alokasi_jam = $(this).attr('data-id-alokasi-jam');

			if (i == limit || (index + 1) == total_ruang){
				// send to background;
				// params_ruang.push(childs_ruang);
				$.ajax({
					url : '<?php echo site_url('jadwal_pelajaran/get_child_ruang_json');?>',
					type : 'post',
					dataType : 'jSON',
					data : {
						ids : childs_ruang
					},
					success : function(response){
						if (typeof (response) && response.success){
							$.each(response.data, function(i, v){
								$('td.drop-area-ruang:eq('+v.index+')').addClass(v.class).html(v.nama_ruang);
								if ((index + 1) == total_ruang){
									makeTable();
								}
							});
						}

						
						// $('th.nama_kelas').css({'width' : '150px'});
					},
					error : function(response){

					}
				});

				childs_ruang = [];
				i = 0;
			}

			childs_ruang[i] = {
				'index' : index,
				'id_detail_kelas' : id_detail_kelas,
				'id_alokasi_jam' : id_alokasi_jam
			};
			i++;
		});
	});
</script>