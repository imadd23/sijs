<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_pelajaran extends My_controller {

	public $authAction;
	protected $jadwalFileName;
	protected $jadwalPath;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_jadwal');
	}

	public function index()
	{
		$this->letMeToAccess('view');

		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('jadwal_pejaran/get_data_json');

		$listTahunAjaran = $this->model_jadwal->getTahunAjaran();
		$id_ref_tahun_ajaran = 0;
		if (!empty($this->input->post('id_ref_tahun_ajaran'))){
			$id_ref_tahun_ajaran = $this->input->post('id_ref_tahun_ajaran');
		}else
		if (!empty($listTahunAjaran)){
			foreach ($listTahunAjaran as $key => $value) {
				if ($value['aktif'] == '1'){
					$id_ref_tahun_ajaran = $value['id_ref_tahun_ajaran'];
					break;
				}
			}
		}
		$listAlokasiJam = $this->model_jadwal->getAlokasiJam($id_ref_tahun_ajaran);
		$listDetailKelas = $this->model_jadwal->getDetailKelas($id_ref_tahun_ajaran);
		$data['listAlokasiJam'] = $listAlokasiJam;
		$data['listDetailKelas'] = $listDetailKelas;
		$data['listGuruMapel'] = $this->model_jadwal->getGuruMapel([
			'GM.id_ref_tahun_ajaran' => $id_ref_tahun_ajaran
			]);
		$data['semester'] = $this->semesterAktif().' Semester '.$this->stringSemesterAktif();
		$this->render_view('view_jadwal_pelajaran', $data);
	}

	public function get_child_json(){
		ob_start();
		$id = $this->input->post('ids');
		$response = array();
		if (!empty($id)){
			$data = array();
			foreach ($id as $key => $value) {
				$jadwal = $this->model_jadwal->getDataByParams(array(
					'J.id_detail_kelas' => $value['id_detail_kelas'],
					'J.id_alokasi_jam' => $value['id_alokasi_jam'],
					));

				if (!empty($jadwal)){
					$data[$key]['class'] = 'dropped-area';
					$data[$key]['kode_guru'] = $jadwal[0]['id_guru'];
					$data[$key]['warna'] = $jadwal[0]['warna'].' !important';
					$data[$key]['detail'] =  'Kode Guru : '.$jadwal[0]['id_guru'].'<br>Nama : '.$jadwal[0]['Nama'].'<br>Mapel : '.$jadwal[0]['nama_mapel'].'<br>Ruang : '.$jadwal[0]['nama_ruang'].'<br>Hari : '.$jadwal[0]['hari'].'<br>Jam : '.$jadwal[0]['jam_mulai'].' s.d '.$jadwal[0]['jam_selesai'];
				}else{
					$data[$key]['class'] = '';
					$data[$key]['kode_guru'] = '';
					$data[$key]['warna'] = 'orange !important';
					$data[$key]['detail'] = '';
				}
				$data[$key]['index'] = $value['index'];
			}
			$response['success'] = true;
			$response['data'] = $data;
		}else{
			$response['success'] = false;
		}

		echo json_encode($response);
	}

	public function get_child_ruang_json(){
		ob_start();
		$id = $this->input->post('ids');
		$response = array();
		if (!empty($id)){
			$data = array();
			foreach ($id as $key => $value) {
				$jadwal = $this->model_jadwal->getDataRuangByParams(array(
					'J.id_detail_kelas' => $value['id_detail_kelas'],
					'J.id_alokasi_jam' => $value['id_alokasi_jam'],
					));

				if (!empty($jadwal[0]['nama_ruang'])){
					$data[$key]['class'] = 'dropped-area-ruang';
					$data[$key]['nama_ruang'] = $jadwal[0]['kode_mapel'].'/'.$jadwal[0]['nama_ruang'];
				}else{
					$data[$key]['class'] = '';
					$data[$key]['nama_ruang'] = '';
				}
				$data[$key]['index'] = $value['index'];
			}
			$response['success'] = true;
			$response['data'] = $data;
		}else{
			$response['success'] = false;
		}

		echo json_encode($response);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : 'jenjang';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'asc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "jenjang LIKE '%".$requestData['search']['value']."%'",
                "nama_kelas LIKE '%".$requestData['search']['value']."%'",
                "nama_jurusan LIKE '%".$requestData['search']['value']."%'",
                "nama_singkat_jurusan LIKE '%".$requestData['search']['value']."%'"
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_jadwal->getListData($options);
        $totalFiltered = $this->model_jadwal->getTotalData($options);
        $totalData = $this->model_jadwal->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('jadwal_pejaran/edit/'.$value->id_jadwal_pejaran).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('jadwal_pejaran/delete/'.$value->id_jadwal_pejaran).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('jadwal_pejaran/edit/'.$value->id_jadwal_pejaran).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('jadwal_pejaran/delete/'.$value->id_jadwal_pejaran).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listTahunAjaran'] = $this->model_jadwal->getTahunAjaran();
		$data['listJenjang'] = $this->model_jadwal->getJenjang();
		$data['listJurusan'] = $this->model_jadwal->getJurusan();
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_kelas', $data);
	}

	public function do_add()
	{
		ob_start();
		$this->letMeToAccess('add');
		$data = [    
			'id_alokasi_jam' => $this->input->post('id_alokasi_jam'),
			'id_detail_kelas' => $this->input->post('id_detail_kelas'),
			'id_guru_mapel' => $this->input->post('id_guru_mapel')
		];

		$isExist = $this->model_jadwal->getDataByParams(array(
				'J.id_detail_kelas' => $this->input->post('id_detail_kelas'),
				'J.id_alokasi_jam' => $this->input->post('id_alokasi_jam'),
			));

		$isBentrok = $this->model_jadwal->getDataByParams(array(
				'G.id_guru' => $this->input->post('id_guru'),
				'J.id_alokasi_jam' => $this->input->post('id_alokasi_jam')
			));

		$isBentrok = (isset($isBentrok[0]['id_guru']) && !empty($isBentrok[0]['id_guru'])) ? true : false;

		if ($isBentrok){
			echo json_encode(array('success' => '0', 'message' => 'Maaf, Guru ini sudah menempati jam ini'));
			return;
		}else
		if ($isExist){
			$add = $this->model_jadwal->update(array(
					'id_guru_mapel' => $this->input->post('id_guru_mapel')), array(
					'id_detail_kelas' => $this->input->post('id_detail_kelas'),
					'id_alokasi_jam' => $this->input->post('id_alokasi_jam')));
		}else{
			$add = $this->model_jadwal->add($data);
		}

		if ($add){
			echo json_encode(array('success' => '1', 'message' => 'Data sukses tersimpan'));
		}else{
			echo json_encode(array('success' => '0', 'message' => 'Data gagal tersimpan'));
		}
		
	}

	public function do_add_ruang()
	{
		ob_start();
		$this->letMeToAccess('add');
		$data = [    
			'id_alokasi_jam' => $this->input->post('id_alokasi_jam'),
			'id_detail_kelas' => $this->input->post('id_detail_kelas'),
			'id_ref_ruang' => $this->input->post('id_ref_ruang')
		];
		$isExist = $this->model_jadwal->getDataRuangByParams(array(
					'J.id_detail_kelas' => $this->input->post('id_detail_kelas'),
					'J.id_alokasi_jam' => $this->input->post('id_alokasi_jam')
					));

		$isBentrok = $this->model_jadwal->getDataByParams(array(
				'J.id_ref_ruang' => $this->input->post('id_ref_ruang'),
				'J.id_alokasi_jam' => $this->input->post('id_alokasi_jam')
			));

		$isBentrok = (isset($isBentrok[0]['id_ref_ruang']) && !empty($isBentrok[0]['id_ref_ruang'])) ? true : false;

		if ($isBentrok){
			echo json_encode(array('success' => '0', 'message' => 'Maaf, Ruang ini sudah dipakai pada jam ini', 'data' => ''));
			return;
		}else
		if ($isExist){
			$add = $this->model_jadwal->update(array(
					'id_ref_ruang' => $this->input->post('id_ref_ruang')), array(
					'id_detail_kelas' => $this->input->post('id_detail_kelas'),
					'id_alokasi_jam' => $this->input->post('id_alokasi_jam')));
		}else{
			$add = $this->model_jadwal->add($data);
		}
				
		if ($add){
			$data = $this->model_jadwal->getDataRuangByParams(array(
				'J.id_detail_kelas' => $this->input->post('id_detail_kelas'),
				'J.id_alokasi_jam' => $this->input->post('id_alokasi_jam')
				));
			$_data = !empty($data) ? $data[0]['kode_mapel'].'/'.$data[0]['nama_ruang'] : '';
			echo json_encode(array('success' => '1', 'message' => 'Data sukses tersimpan', 'data' => $_data));
		}else{
			echo json_encode(array('success' => '0', 'message' => 'Data gagal tersimpan', 'data' => ''));
		}
	}

	public function edit()
	{
		$this->letMeToAccess('edit');
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('jadwal_pejaran/get_data_json');

		$listTahunAjaran = $this->model_jadwal->getTahunAjaran();
		$id_ref_tahun_ajaran = 0;
		if (!empty($this->input->post('id_ref_tahun_ajaran'))){
			$id_ref_tahun_ajaran = $this->input->post('id_ref_tahun_ajaran');
		}else
		if (!empty($listTahunAjaran)){
			foreach ($listTahunAjaran as $key => $value) {
				if ($value['aktif'] == '1'){
					$id_ref_tahun_ajaran = $value['id_ref_tahun_ajaran'];
					break;
				}
			}
		}
		$listAlokasiJam = $this->model_jadwal->getAlokasiJam($id_ref_tahun_ajaran);
		$listDetailKelas = $this->model_jadwal->getDetailKelas($id_ref_tahun_ajaran);
		$data['listAlokasiJam'] = $listAlokasiJam;
		$data['listDetailKelas'] = $listDetailKelas;
		$data['listGuruMapel'] = $this->model_jadwal->getGuruMapel([
			'GM.id_ref_tahun_ajaran' => $id_ref_tahun_ajaran
			]);
		$data['semester'] = $this->semesterAktif().' Semester '.$this->stringSemesterAktif();
		$this->render_view('view_edit_jadwal_pelajaran', $data);
	}

	public function edit_ruang()
	{
		$this->letMeToAccess('edit');
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('jadwal_pejaran/get_data_ruang_json');

		$listTahunAjaran = $this->model_jadwal->getTahunAjaran();
		$id_ref_tahun_ajaran = 0;
		if (!empty($this->input->post('id_ref_tahun_ajaran'))){
			$id_ref_tahun_ajaran = $this->input->post('id_ref_tahun_ajaran');
		}else
		if (!empty($listTahunAjaran)){
			foreach ($listTahunAjaran as $key => $value) {
				if ($value['aktif'] == '1'){
					$id_ref_tahun_ajaran = $value['id_ref_tahun_ajaran'];
					break;
				}
			}
		}
		$listAlokasiJam = $this->model_jadwal->getAlokasiJam($id_ref_tahun_ajaran);
		$listDetailKelas = $this->model_jadwal->getDetailKelas($id_ref_tahun_ajaran);
		$data['listAlokasiJam'] = $listAlokasiJam;
		$data['listDetailKelas'] = $listDetailKelas;
		$data['listRuang'] = $this->model_jadwal->getRuang();
		$data['semester'] = $this->semesterAktif().' Semester '.$this->stringSemesterAktif();
		$this->render_view('view_edit_ruang_jadwal_pelajaran', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('jadwal_pejaran'));
		}
		$data = [
			'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
			'id_ref_jenjang' => $this->input->post('id_ref_jenjang'),
			'id_ref_jurusan' => $this->input->post('id_ref_jurusan'),
			'nama_kelas' => $this->input->post('nama_kelas')
		];
		$update = $this->model_jadwal->update($data, ['id_jadwal_pejaran' => $this->input->post('id_jadwal_pejaran')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('jadwal_pejaran'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('jadwal_pejaran/edit/'.$this->input->post('id_jadwal_pejaran')));
		}		
	}

	public function do_delete()
	{
		ob_start();
		$this->letMeToAccess('delete');
		$id_alokasi_jam = $this->input->post('id_alokasi_jam');
		$id_detail_kelas = $this->input->post('id_detail_kelas');

		$delete = $this->model_jadwal->delete($id_alokasi_jam, $id_detail_kelas);
		if ($delete){
			$response = array('success' => true);
		}else{
			$response = array('success' => false);
		}	
		echo json_encode($response);
	}

	public function do_delete_ruang()
	{
		ob_start();
		$this->letMeToAccess('delete');
		$id_alokasi_jam = $this->input->post('id_alokasi_jam');
		$id_detail_kelas = $this->input->post('id_detail_kelas');

		$delete = $this->model_jadwal->delete_ruang($id_alokasi_jam, $id_detail_kelas);
		if ($delete){
			$response = array('success' => true);
		}else{
			$response = array('success' => false);
		}	
		echo json_encode($response);
	}

	private function columns(){
		$cols = array(
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
			'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
			'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
			'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
			'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
			'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
			'FA', 'FB', 'FC', 'FD', 'FF', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
		);
		return $cols;
	}

	public function export(){
		/** unlimited time */
		set_time_limit(0);

		/** Include path **/
		include APPPATH.'/libraries/excel/PHPExcel.php';

		$this->isDirectToDownload();
		/** PHPExcel_Writer_Excel2007 */
		// include 'PHPExcel/Writer/Excel2007.php';

		$objPHPExcel = new PHPExcel();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$stringSemesterAktif = strtoupper($this->stringSemesterAktif());
		$semesterAktif = strtoupper($this->semesterAktif());

		$objPHPExcel->getProperties()->setCreator("SMKN 1 Kersana");
		$objPHPExcel->getProperties()->setLastModifiedBy("SMKN 1 Kersana");
		$objPHPExcel->getProperties()->setTitle("Jadwal Pelajaran SMKN 1 Kersana");
		$objPHPExcel->getProperties()->setSubject("Jadwal Pelajaran SMKN 1 Kersana");
		$objPHPExcel->getProperties()->setDescription("Jadwal Pelajaran SMKN 1 Kersana");

		// init var
		$id_ref_tahun_ajaran = $this->idSemesterAktif();
		$listAlokasiJam = $this->model_jadwal->getAlokasiJam($id_ref_tahun_ajaran);
		$listDetailKelas = $this->model_jadwal->getDetailKelas($id_ref_tahun_ajaran);
		$hari = array();
		$kodeJam = array();
		$aDataJam = array();
		$highestColumn = '';
		$center = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $border = array(
		  	'borders' => array(
		    	'allborders' => array(
		      		'style' => PHPExcel_Style_Border::BORDER_THIN
		    	)
		  	)
		);
		$bgBrown = array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '7f7f7f')
	        )
	    );
		// end init var
		if (!empty($listAlokasiJam)){
			$_highestColumn = 3;
			foreach ($listAlokasiJam as $key => $value) {
				$fromColumn = $_highestColumn + 1;
				$_highestColumn = $_highestColumn + $value['jumlah_data'];
				$hari[$key] = array(
					'colspan' => $value['jumlah_data'],
					'fromColumn' => $this->columns()[$fromColumn],
					'highestColumn' => $this->columns()[$_highestColumn],
					'hari' => strtoupper($value['hari'])
					);
				
				$_jam = str_replace("#", '"', $value['jam']);
				$jam = json_decode($_jam, true);
				if (!empty($jam)){
					foreach ($jam as $key => $value) {
						$aDataJam[] = $value['id'];
						$kodeJam[] = $value['kode_jam'];
					}
				}
			}
			$highestColumn = $this->columns()[$_highestColumn];
		}
		$objPHPExcel->setActiveSheetIndex(0);
		// set title 1
		$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$highestColumn.'1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:'.$highestColumn.'2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:'.$highestColumn.'3');

		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'JADWAL PELAJARAN SEMESTER '.$stringSemesterAktif);
		$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'SMK NEGERI 1 KERSANA KABUPATEN BREBES');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'TAHUN PELAJARAN '.$semesterAktif);
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.$highestColumn.'1')->applyFromArray($center);
		$objPHPExcel->getActiveSheet()->getStyle('A2:'.$highestColumn.'2')->applyFromArray($center);
		$objPHPExcel->getActiveSheet()->getStyle('A3:'.$highestColumn.'3')->applyFromArray($center);
		// end title 1
		// set title 2
		$objPHPExcel->getActiveSheet()->mergeCells('A5:D6');
		$objPHPExcel->getActiveSheet()->SetCellValue('A5', 'NAMA KELAS');
		// end title 2

		// set wraptext nama kelas
		$objPHPExcel->getActiveSheet()->getStyle('A5:D6')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('A5:D6')->applyFromArray($center);
		$objPHPExcel->getActiveSheet()->getStyle('A5:D6')->applyFromArray($border);
		$objPHPExcel->getActiveSheet()->getStyle('A5:D6')->applyFromArray($bgBrown);
		// end set wraptext nama kelas

		// set width
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(2);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2);
		// end set width
		// set hari
		if (!empty($hari)){
			foreach ($hari as $key => $value) {
				// set width
				if ($key == 0){
					$range = range(4, $_highestColumn);
				}
				if ($key == 0 && !empty($range)){
					foreach ($range as $k => $v) {
						$c = $this->columns()[$v];
						$objPHPExcel->getActiveSheet()->getColumnDimension($c)->setWidth(5.57);
					}
				}
				// end set width
				$objPHPExcel->getActiveSheet()->mergeCells($value['fromColumn'].'5:'.$value['highestColumn'].'5');
				$objPHPExcel->getActiveSheet()->SetCellValue($value['fromColumn'].'5', $value['hari']);
				$objPHPExcel->getActiveSheet()->getStyle($value['fromColumn'].'5:'.$value['highestColumn'].'5')->applyFromArray($center);
				$objPHPExcel->getActiveSheet()->getStyle($value['fromColumn'].'5:'.$value['highestColumn'].'5')->applyFromArray($border);
				$objPHPExcel->getActiveSheet()->getStyle($value['fromColumn'].'5:'.$value['highestColumn'].'5')->applyFromArray($bgBrown);
			}
		}
		// end set hari

		// set jam
		$highestJam = count($kodeJam) + 4;
		$j = 0;
		for ($i = 4; $i < $highestJam; $i++) { 
			$jamColumn = $this->columns()[$i];
			$objPHPExcel->getActiveSheet()->SetCellValue($jamColumn.'6', $kodeJam[$j]);
			$objPHPExcel->getActiveSheet()->getStyle($jamColumn.'6')->applyFromArray($center);
			$objPHPExcel->getActiveSheet()->getStyle($jamColumn.'6')->applyFromArray($border);
			$objPHPExcel->getActiveSheet()->getStyle($jamColumn.'6')->applyFromArray($bgBrown);
			$j++;
		}
		// end set jam

		// init var kelas
		$highestRow = 0;
		if (!empty($listDetailKelas)){
			$rowKelas = 7;
			$rowJenjang = 7;
			$rowDetailKelas = 7;
			foreach ($listDetailKelas as $key => $value) {
				$_aJenjang = str_replace('#', '"', $value['jenjang']);
				$aJenjang = json_decode($_aJenjang, true);
				$rowspanJurusan = $value['jumlah_data'] * 2;
				$rowBegin = ($key == 0) ? $rowKelas : ($rowKelas + 1);
				$rowKelas = ($key == 0) ? ($rowKelas + $rowspanJurusan - 1) : ($rowKelas + $rowspanJurusan);
				$highestRow = $rowKelas;

				$objPHPExcel->getActiveSheet()->mergeCells('A'.$rowBegin.':A'.$rowKelas);
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowBegin, $value['nama_jurusan']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowBegin.':A'.$rowKelas)->getAlignment()->setTextRotation(90);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowBegin.':A'.$rowKelas)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowBegin.':A'.$rowKelas)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				if (!empty($aJenjang)){
					foreach ($aJenjang as $k => $v) {
						$aId = $this->model_jadwal->getJenjangById($value['id_ref_jurusan'], $v['id_jenjang'], $value['id_ref_tahun_ajaran']);
						$rowspanJenjang = $v['jumlah_data'] * 2;
						$rowJenjangBegin = ($key == 0 && $k == 0) ? 7 : ($rowJenjang + 1);
						$rowJenjang = ($key == 0 && $k == 0) ? ($rowJenjang + $rowspanJenjang - 1) : ($rowJenjang + $rowspanJenjang);

						$objPHPExcel->getActiveSheet()->mergeCells('B'.$rowJenjangBegin.':B'.$rowJenjang);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowJenjangBegin, $v['jenjang']);
						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowJenjangBegin.':B'.$rowJenjang)->getAlignment()->setTextRotation(90);
						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowJenjangBegin.':B'.$rowJenjang)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowJenjangBegin.':B'.$rowJenjang)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						if (!empty($aId)){
							foreach ($aId as $kk => $vv) {
								$dKelas = $this->model_jadwal->getDetailKelasById($vv['id_detail_kelas']);
								$rowspanDetailKelas = 2;
								$rowDetailKelasBegin = ($key == 0 && $k == 0 && $kk == 0) ? 7 : ($rowDetailKelas + 1);
								$rowDetailKelas = ($key == 0 && $k == 0 && $kk == 0) ? ($rowDetailKelas + $rowspanDetailKelas - 1) : ($rowDetailKelas + $rowspanDetailKelas);

								if (!empty($dKelas)){
									$objPHPExcel->getActiveSheet()->mergeCells('C'.$rowDetailKelasBegin.':C'.$rowDetailKelas);
									$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowDetailKelasBegin, $dKelas['nama_singkat_jurusan']);
									$objPHPExcel->getActiveSheet()->getStyle('C'.$rowDetailKelasBegin.':C'.$rowDetailKelas)->getAlignment()->setTextRotation(90);
									$objPHPExcel->getActiveSheet()->getStyle('C'.$rowDetailKelasBegin.':C'.$rowDetailKelas)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
									$objPHPExcel->getActiveSheet()->getStyle('C'.$rowDetailKelasBegin.':C'.$rowDetailKelas)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$objPHPExcel->getActiveSheet()->mergeCells('D'.$rowDetailKelasBegin.':D'.$rowDetailKelas);
									$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowDetailKelasBegin, $dKelas['nama_kelas']);
									$objPHPExcel->getActiveSheet()->getStyle('D'.$rowDetailKelasBegin.':D'.$rowDetailKelas)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
									$objPHPExcel->getActiveSheet()->getStyle('D'.$rowDetailKelasBegin.':D'.$rowDetailKelas)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									if (!empty($aDataJam)){
										$i = 4;
										$_col = $this->columns();
										foreach ($aDataJam as $kkk => $vvv) {
											$jadwal = $this->model_jadwal->getDataByParams(array(
												'J.id_detail_kelas' => $vv['id_detail_kelas'],
												'J.id_alokasi_jam' => $vvv,
												));
											$rowGuru = $rowDetailKelasBegin;
											$rowRuang = $rowDetailKelasBegin + 1;

											if (!empty($jadwal) && array_key_exists($i, $_col)){
												$jamColumn = $_col[$i];
												$guru = $jadwal[0]['id_guru'];
												$id_guru_mapel = $jadwal[0]['id_guru_mapel'];
												$id_alokasi_jam = $jadwal[0]['id_alokasi_jam'];
												$id_detail_kelas = $jadwal[0]['id_detail_kelas'];
												$warna = $jadwal[0]['warna'];
												$mapel = !empty($jadwal[0]['nama_ruang']) ? $jadwal[0]['kode_mapel'].'/'.$jadwal[0]['nama_ruang'] : '';

												if (!empty($id_guru_mapel)){
													$indexRuang = $id_guru_mapel.'-'.$id_alokasi_jam.'-'.$id_detail_kelas; 
													$buildRuang[$indexRuang][] = 1;
												}
												
												$objPHPExcel->getActiveSheet()->SetCellValue($jamColumn.$rowGuru, $guru);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->applyFromArray(
												    array(
												        'fill' => array(
												            'type' => PHPExcel_Style_Fill::FILL_SOLID,
												            'color' => array('rgb' => str_replace('#', '', $warna))
												        )
												    )
												);
												$objPHPExcel->getActiveSheet()->SetCellValue($jamColumn.$rowRuang, $mapel);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->applyFromArray($center);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->applyFromArray($center);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->applyFromArray($border);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->applyFromArray($border);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->getFont()->setSize(8);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->getFont()->setSize(8);
											}else
											if (array_key_exists($i, $_col)){
												$jamColumn = $_col[$i];
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->applyFromArray($center);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->applyFromArray($center);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->applyFromArray($border);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->applyFromArray($border);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowGuru)->getFont()->setSize(8);
												$objPHPExcel->getActiveSheet()->getStyle($jamColumn.$rowRuang)->getFont()->setSize(8);
											}

											$i++;
										}
										// end loop aDataJam
									}
									// end if aDataJam
								}
								// end if dKelas
							}
							// end lopp aId
						}
						// end if aId
					}
					// end loop aJenjang
				}
				// end if aJenjang
				// make server take a rest and reduce overhit
				sleep(2);
			}
			// end loop listDetailKelas
		}
		// end if listDetailKelas

		// kode guru
		$rowKodeGuru = $highestRow + 11;
		$highestColumnKodeGuru = $this->columns()[31];
		$listKodeGuru = $this->model_jadwal->getGuruMapelDistinct(array(
			'GM.id_ref_tahun_ajaran' => $id_ref_tahun_ajaran
			));
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowKodeGuru, 'KODE GURU');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.($rowKodeGuru + 1), 'SMKN 1 KERSANA');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.($rowKodeGuru + 2), 'TAHUN PELAJARAN '.$this->semesterAktif());
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$rowKodeGuru.':'.$highestColumnKodeGuru.$rowKodeGuru);
		$objPHPExcel->getActiveSheet()->mergeCells('F'.($rowKodeGuru + 1).':'.$highestColumnKodeGuru.($rowKodeGuru + 1));
		$objPHPExcel->getActiveSheet()->mergeCells('F'.($rowKodeGuru + 2).':'.$highestColumnKodeGuru.($rowKodeGuru + 2));
		$objPHPExcel->getActiveSheet()->getStyle('F'.$rowKodeGuru)->applyFromArray($center);
		$objPHPExcel->getActiveSheet()->getStyle('F'.($rowKodeGuru + 1))->applyFromArray($center);
		$objPHPExcel->getActiveSheet()->getStyle('F'.($rowKodeGuru + 2))->applyFromArray($center);

		$limitColumnKodeGuru = !empty($listKodeGuru) ? ceil(count($listKodeGuru) / 3) : 0;
		$rowKodeGuru = $rowKodeGuru + 4;
		$_listKodeGuru = array();
		if (!empty($listKodeGuru)){
			// split array kode guru
			for ($i = 2; $i >= 0; $i--) { 
				$offset = ($i == 0) ? 0 : (($limitColumnKodeGuru * $i) - 1);
				$_listKodeGuru[$i] = array_splice($listKodeGuru, $offset);
			}	
		}

		if (!empty($_listKodeGuru)){
			$column = array(array('F', 'G', 'H', 'M'), array('O', 'P', 'Q', 'V'), array('X', 'Y', 'Z', 'AE'));
			$no = 1;
			for ($i = 0; $i <= 2; $i++) {
				$_rowKodeGuru = $rowKodeGuru;
				if (!empty($_listKodeGuru[$i])){
					foreach ($_listKodeGuru[$i] as $key => $value) {
						if ($key == 0){
							$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][0].$_rowKodeGuru, "No");
							$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][1].$_rowKodeGuru, "Kode Guru");
							$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][2].$_rowKodeGuru, "Nama Guru");
							$objPHPExcel->getActiveSheet()->mergeCells($column[$i][2].$_rowKodeGuru.':'.$column[$i][3].$_rowKodeGuru);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->applyFromArray($center);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->applyFromArray($center);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][2].$_rowKodeGuru)->applyFromArray($center);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->getAlignment()->setWrapText(true);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->getAlignment()->setWrapText(true);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][2].$_rowKodeGuru)->getAlignment()->setWrapText(true);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][2].$_rowKodeGuru)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->applyFromArray($border);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->applyFromArray($border);
							$objPHPExcel->getActiveSheet()->getStyle($column[$i][2].$_rowKodeGuru.':'.$column[$i][3].$_rowKodeGuru)->applyFromArray($border);
							$_rowKodeGuru++;
						}
						$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][0].$_rowKodeGuru, $no);
						$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][1].$_rowKodeGuru, $value['id_guru']);
						$objPHPExcel->getActiveSheet()->SetCellValue($column[$i][2].$_rowKodeGuru, $value['Nama']);
						$objPHPExcel->getActiveSheet()->mergeCells($column[$i][2].$_rowKodeGuru.':'.$column[$i][3].$_rowKodeGuru);
						$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->applyFromArray($center);
						$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->applyFromArray($center);
						$objPHPExcel->getActiveSheet()->getStyle($column[$i][0].$_rowKodeGuru)->applyFromArray($border);
						$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->applyFromArray($border);
						$objPHPExcel->getActiveSheet()->getStyle($column[$i][2].$_rowKodeGuru.':'.$column[$i][3].$_rowKodeGuru)->applyFromArray($border);

						$objPHPExcel->getActiveSheet()->getStyle($column[$i][1].$_rowKodeGuru)->applyFromArray(
						    array(
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            'color' => array('rgb' => str_replace('#', '', $value['warna']))
						        )
						    )
						);
						$_rowKodeGuru++;
						$no++;
					}
				}
			}
		}
		// die();
		// end kode guru

		// set freeze column end row
		$objPHPExcel->getActiveSheet()->freezePane('E7');

		$objPHPExcel->getActiveSheet()->setTitle('JADWAL');

        // Write file to the browser
        $objWriter->save($this->jadwalPath.$this->jadwalFileName);
        header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="' . $this->jadwalFileName . '"');
		readfile($this->jadwalPath.$this->jadwalFileName);
		exit();
	}

	public function isDirectToDownload(){
		$stringSemesterAktif = strtoupper($this->stringSemesterAktif());
		$semesterAktif = strtoupper($this->semesterAktif());
		$this->jadwalPath = FCPATH."files/jadwal/";
		$lastUpdated = date('YmdHis', strtotime($this->model_jadwal->get_last_updated()));
		$this->jadwalFileName = str_replace('/', '-', 'jadwal_smkn1kersana_'.$semesterAktif.'_'.$stringSemesterAktif.'_'.$lastUpdated.'.xls');
		$pathFile = $this->jadwalPath.$this->jadwalFileName;
		if (!is_dir($this->jadwalPath)){
			mkdir(FCPATH."files");
			mkdir(FCPATH."files/jadwal/");
			chmod($this->jadwalPath, 777);
		}		
		if (!file_exists($pathFile)){
			return true;
		}else{
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="' . $this->jadwalFileName . '"');
			readfile($pathFile);
			exit();
		}
	}
}
