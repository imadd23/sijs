<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

	protected $table_name = 'login';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT A.*, B.role_name, C.Nama, C.Nip 
        	FROM 
        	$this->table_name AS A
            LEFT JOIN
            auth_role AS B
            ON A.role_id = B.role_id
            LEFT JOIN guru AS C
            ON A.id_guru = C.id_guru ";
        return $query;
    }

    public function getListData($options = []){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like."
            ORDER BY ".$options['order']." ".$options['mode']."
            LIMIT ".$options['offset'].", ".$options['limit'];

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTotalData($options){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getTotal(){
        $sql = $this->query();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getListRole(){
    	$result = $this->db->get('auth_role')->result_array();
    	return $result;
    }

    public function getListGuru(){
        $result = $this->db->get('guru')->result_array();
        return $result;
    }

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->get_where($this->table_name, ['id_logkar' => $id])->row_array();
		return $result;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('id_logkar', $id)->delete($this->table_name);
		return $delete;
	}
}
