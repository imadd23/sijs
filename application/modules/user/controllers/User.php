<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_user');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('user/get_data_json');
		$this->render_view('view_user', $data);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : 'jenjang';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'asc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "role_name LIKE '%".$requestData['search']['value']."%'",
                "Nama LIKE '%".$requestData['search']['value']."%'",
                "Nip LIKE '%".$requestData['search']['value']."%'",
                "email LIKE '%".$requestData['search']['value']."%'",
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_user->getListData($options);
        $totalFiltered = $this->model_user->getTotalData($options);
        $totalData = $this->model_user->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('user/edit/'.$value->id_logkar).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('user/delete/'.$value->id_logkar).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('user/edit/'.$value->id_logkar).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('user/delete/'.$value->id_logkar).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listRole'] = $this->model_user->getListRole();
		$data['listGuru'] = $this->model_user->getListGuru();
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['post'] = $this->session->flashdata('post');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_user', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('user'));
		}

		if (empty($_POST['password']) || empty($_POST['repeat_password']) || empty($_POST['email']) || empty($_POST['role_id'])){
			$this->session->set_flashdata('pesan', ['danger', 'Inputan tidak boleh kosong']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/add'));
		}

		if ($_POST['password'] != $_POST['repeat_password']){
			$this->session->set_flashdata('pesan', ['danger', 'Password tidak konsisten']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/add'));
		}

		$add = $this->model_user->add(array(
			'id_guru' => $this->input->post('id_guru'),
			'email' => $this->input->post('email'), 
			'password' => MD5($this->input->post('password')),
			'role_id' => $this->input->post('role_id')  
			));
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('user'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_user->getDataById($id);
		}
		$this->render_view('view_edit_user', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('user'));
		}

		if (empty($_POST['id_guru']) || empty($_POST['email']) || empty($_POST['role_id'])){
			$this->session->set_flashdata('pesan', ['danger', 'Inputan tidak boleh kosong']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/edit/'.$this->input->post('id_logkar')));
		}

		if ($_POST['password'] != $_POST['repeat_password']){
			$this->session->set_flashdata('pesan', ['danger', 'Password tidak konsisten']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/edit/'.$this->input->post('id_logkar')));
		}

		$data = array(
			'id_guru' => $this->input->post('id_guru'),
			'email' => $this->input->post('email'),
			'role_id' => $this->input->post('role_id')  
			);

		if (!empty($_POST['password'])){
			$data['password'] = MD5($this->input->post('password'));
		}

		$update = $this->model_user->update($data, array(
			'id_logkar' => $this->input->post('id_logkar')
			));

		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('user'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('user/edit/'.$this->input->post('id_logkar')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_user->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('user'));
	}
}
