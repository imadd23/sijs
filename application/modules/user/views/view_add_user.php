<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Tambah User</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('user/do_add', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Guru</label>

				<div class="col-sm-3">
					<select id="id_guru" name="id_guru" class="input-sm" />
						<?php
						if (!empty($listGuru)){
							foreach ($listGuru as $key => $value) {
								if (isset($post) && $post['id_guru'] == $value['id_guru']){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}

								echo '<option value="'.$value['id_guru'].'" '.$selected.'>'.$value['Nip'].' | '.$value['Nama'].'</option>';
							}
						}						
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email</label>

				<div class="col-sm-3">
					<input type="text" name="email" class="form-control input-sm" value="<?php echo (isset($post)) ? $post['email'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password</label>

				<div class="col-sm-3">
					<input type="password" name="password" class="form-control input-sm" value="<?php echo (isset($post)) ? $post['password'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ulangi Password</label>

				<div class="col-sm-3">
					<input type="password" name="repeat_password" class="form-control input-sm" value="<?php echo (isset($post)) ? $post['repeat_password'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Group Akses</label>

				<div class="col-sm-3">
					<select id="role_id" name="role_id" class="input-sm" />
						<?php
						if (!empty($listRole)){
							foreach ($listRole as $key => $value) {
								if (isset($post) && $post['role_id'] == $value['role_id']){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}

								echo '<option value="'.$value['role_id'].'" '.$selected.'>'.$value['role_name'].'</option>';
							}
						}						
						?>
					</select>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->