<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_guru extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_ref_guru');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		
		$data['ta'] = $this->model_ref_guru->getData();
		$data['pesan'] = $this->session->flashdata('pesan');
		$this->render_view('view_guru', $data);
	}

	private function dataConstruct(){
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_guru', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_guru'));
		}
		$data = [
			'id_guru' => $this->input->post('id_guru'),
			'Nama' => $this->input->post('Nama'),
			'Nip' => $this->input->post('Nip')
		];
		$add = $this->model_ref_guru->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('ref_guru'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_guru/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_ref_guru->getDataById($id);
		}
		$this->render_view('view_edit_guru', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_guru'));
		}
		$data = [
			'Nama' => $this->input->post('Nama'),
			'Nip' => $this->input->post('Nip')
		];
		$update = $this->model_ref_guru->update($data, ['id_guru' => $this->input->post('id_guru')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('ref_guru'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_guru/edit/'.$this->input->post('id_guru')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_ref_guru->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('ref_guru'));
	}
}
