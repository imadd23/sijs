<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Ubah Referensi Ruang</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('ref_ruang/do_update', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Ruang </label>

				<div class="col-sm-3">
					<input type="text" name="nama_ruang" class="form-control" value="<?php echo (isset($post)) ? $post['nama_ruang'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Kelas </label>

				<div class="col-sm-3">
					<input type="text" name="nama_kelas" class="form-control" value="<?php echo (isset($post)) ? $post['nama_kelas'] : '';?>">
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<input type="hidden" name="id_ref_ruang" value="<?php echo (isset($post)) ? $post['id_ref_ruang'] : 0;?>">

					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->