<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ref_ruang extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_ref_ruang');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		
		$data['ta'] = $this->model_ref_ruang->getData();
		$data['pesan'] = $this->session->flashdata('pesan');
		$this->render_view('view_ruang', $data);
	}

	private function dataConstruct(){
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_ruang', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_ruang'));
		}
		$data = [
			'nama_ruang' => $this->input->post('nama_ruang'),
			'nama_kelas' => $this->input->post('nama_kelas')
		];
		$add = $this->model_ref_ruang->add($data);
		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('ref_ruang'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_ruang/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_ref_ruang->getDataById($id);
		}
		$this->render_view('view_edit_ruang', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('ref_ruang'));
		}
		$data = [
			'nama_ruang' => $this->input->post('nama_ruang'),
			'nama_kelas' => $this->input->post('nama_kelas')
		];
		$update = $this->model_ref_ruang->update($data, ['id_ref_ruang' => $this->input->post('id_ref_ruang')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('ref_ruang'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('ref_ruang/edit/'.$this->input->post('id_ref_ruang')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_ref_ruang->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('ref_ruang'));
	}
}
