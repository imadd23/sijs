<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Ubah Group</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('group/do_update', ['class' => 'form-horizontal', 'role' => 'form']);?>
			
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Group</label>

				<div class="col-sm-3">
					<input type="text" name="role_name" class="form-control input-sm" value="<?php echo (isset($post)) ? $post['role_name'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Hak Akses</label>

				<div class="col-sm-9" style="max-height:250px; overflow-y: scroll;">
					<?php
					if (!empty($listPage)){
						foreach ($listPage as $key => $value) {
							if (!empty($preventPage) && in_array($value->page_id, $preventPage)){
								$checked = 'checked';
							}else{
								$checked = '';
							}

							echo '<div class="checkbox">
										<label>
											<input name="page_id[]" value="'.$value->page_id.'" class="ace" type="checkbox" '.$checked.'>
											<span class="lbl"> '.$value->page.' > '.$value->action.'</span>
										</label>
									</div>';
						}
					}
					?>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<input type="hidden" name="role_id" value="<?php echo (isset($post)) ? $post['role_id'] : 0;?>">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->