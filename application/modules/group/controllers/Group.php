<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_group');
	}

	public function index()
	{
		$this->letMeToAccess('view');
		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('group/get_data_json');
		$this->render_view('view_group', $data);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : 'jenjang';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'asc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "role_name LIKE '%".$requestData['search']['value']."%'"
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_group->getListData($options);
        $totalFiltered = $this->model_group->getTotalData($options);
        $totalData = $this->model_group->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

				$hakAkses = $this->model_group->getListPageByRoleId($value->role_id);
				$_hakAkses = array();
				if (!empty($hakAkses)){
					foreach ($hakAkses as $k => $v) {
						$_hakAkses[] = $v->page;
					}
				}
				$value->hak_akses = '<ul><li>'.implode('</li><li>', $_hakAkses).'</li></ul>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('group/edit/'.$value->role_id).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('group/delete/'.$value->role_id).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('group/edit/'.$value->role_id).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('group/delete/'.$value->role_id).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listPage'] = $this->model_group->getListPage();
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_group', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('group'));
		}

		$this->db->trans_start();

		$result = $this->model_group->add(array('role_name' => $this->input->post('role_name')));
		$role_id = $this->db->insert_id();

		$page_ids = $this->input->post('page_id');
		if (!empty($page_ids)){
			foreach ($page_ids as $key => $value) {
				$data[$key] = array('role_id' => $role_id, 'page_id' => $value);
			}
		}

		$result_batch = $this->db->insert_batch('auth_role_page', $data); 
		$add = $result && $result_batch;
		if ($add){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}

		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('group'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('group/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		$data['preventPage'] = $this->model_group->getDataPageById($id);
		if (empty($data['post'])){
			$data['post'] = $this->model_group->getDataById($id);
		}
		$this->render_view('view_edit_group', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('group'));
		}
		$preventPage = $this->model_group->getDataPageById($this->input->post('role_id'));
		$page_ids =  !empty($this->input->post('page_id')) ? $this->input->post('page_id') : array();

		$this->db->trans_start();
		$result = $this->model_group->update(array('role_name' => $this->input->post('role_name')), array('role_id' => $this->input->post('role_id')));
		$deleteAllRole = $this->db->where('role_id', $this->input->post('role_id'))->delete('auth_role_page');

		if (!empty($preventPage)){
			foreach ($preventPage as $key => $value) {
				if (!in_array($value, $page_ids)){
					unset($preventPage[$key]);
				}
			}
		}

		$joinedPages = array_merge($preventPage, $page_ids);
		$joinedPages = array_unique($joinedPages);

		if (!empty($joinedPages)){
			foreach ($joinedPages as $key => $value) {
				$data[$key] = array('role_id' => $this->input->post('role_id'), 'page_id' => $value);
			}
			$result_batch = $this->db->insert_batch('auth_role_page', $data); 
		}else{
			$result_batch = true;
		}
		
		$add = $result && $result_batch;
		if ($add){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}

		if ($add){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('group'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('group/edit/'.$this->input->post('role_id')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_group->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('group'));
	}
}
