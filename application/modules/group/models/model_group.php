<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_group extends CI_Model {

	protected $table_name = 'auth_role';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT * 
        	FROM 
        	$this->table_name";
        return $query;
    }

    public function getListData($options = []){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like."
            ORDER BY ".$options['order']." ".$options['mode']."
            LIMIT ".$options['offset'].", ".$options['limit'];

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTotalData($options){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getListPage(){
    	$result = $this->db->query("SELECT * FROM auth_page ORDER BY page, FIELD(action, 'view', 'add', 'edit', 'delete')")->result();
    	return $result;
    }

    public function getListPageByRoleId($id){
    	$result = $this->db->query("SELECT CONCAT(page, '(', GROUP_CONCAT(action ORDER BY FIELD(action, 'view', 'add', 'edit', 'delete')),')') AS page 
    		FROM auth_role_page 
    		LEFT JOIN auth_page
    		ON auth_page.page_id = auth_role_page.page_id
    		LEFT JOIN auth_role
    		ON auth_role.role_id = auth_role_page.role_id
    		WHERE
    			auth_role_page.role_id = '$id'
    		GROUP BY page
    		ORDER BY page")->result();
    	return $result;
    }

    public function getTotal(){
        $sql = $this->query();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->get_where($this->table_name, ['role_id' => $id])->row_array();
		return $result;
	}

	public function getDataPageById($id)
	{
		$result = $this->db->select('page_id')
			->from('auth_role_page')
			->where(['role_id' => $id])
			->get()
			->result_array();
		$results = array();
		if (!empty($result)){
			foreach ($result as $key => $value) {
				$results[] = $value['page_id'];
			}
		}
		return $results;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('role_id', $id)->delete($this->table_name);
		return $delete;
	}
}
