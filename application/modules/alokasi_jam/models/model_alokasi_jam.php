<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_alokasi_jam extends CI_Model {

	protected $table_name = 'alokasi_jam';

	public function __construct(){
		parent::__construct();
	}

	private function query(){
        $query = "SELECT * FROM
        (
        	SELECT CONCAT(tahun_mulai, '/', `tahun_selesai`) AS ta, AJ.jam_mulai, AJ.jam_selesai, AJ.hari, AJ.id_ref_tahun_ajaran, AJ.id_alokasi_jam, 
        	AJ.kode_jam, IF(AJ.is_istirahat = '1', 'Ya', 'Bukan') AS is_istirahat, tahun_mulai, tahun_selesai 
			FROM $this->table_name AS AJ
			LEFT JOIN 
			`ref_tahun_ajaran` AS RTA
			ON `RTA`.`id_ref_tahun_ajaran` = AJ.`id_ref_tahun_ajaran`
		) AS temp1 ";
        return $query;
    }

    public function getListData($options = []){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like.( empty($options['order']) ? ' ' : "
            ORDER BY ".$options['order']." ".$options['mode'])." 
            LIMIT ".$options['offset'].", ".$options['limit'];

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTotalData($options){
        $where_like = empty($options['where_like']) ? '1 = 1' : '('.implode(' OR ', $options['where_like']).')'; 
        $sql = $this->query()."         
            WHERE 
                1 = 1 AND ".$where_like;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function getTotal(){
        $sql = $this->query();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	public function getData()
	{
		$result = $this->db->get($this->table_name)->result();
		return $result;
	}

	public function getTahunAjaran()
	{
		$result = $this->db->select("CONCAT(tahun_mulai, '/', tahun_selesai) AS ta, ref_tahun_ajaran.*")->from('ref_tahun_ajaran')->get()->result_array();
		return $result;
	}

	public function getDataById($id)
	{
		$result = $this->db->select("CONCAT(tahun_mulai, '/', tahun_selesai) AS ta, ".$this->table_name.".*")->from($this->table_name)
			->join('ref_tahun_ajaran', $this->table_name.'.id_ref_tahun_ajaran = ref_tahun_ajaran.id_ref_tahun_ajaran', 'left')
			->where(['id_alokasi_jam' => $id])->get()->row_array();
		return $result;
	}

	public function add($data)
	{
		$add = $this->db->insert($this->table_name, $data);
		return $add;
	}

	public function update($data, $wheres)
	{
		$update = $this->db->update($this->table_name, $data, $wheres);
		return $update;
	}

	public function delete($id){
		$delete = $this->db->where('id_alokasi_jam', $id)->delete($this->table_name);
		return $delete;
	}
}
