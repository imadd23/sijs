<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alokasi_jam extends My_controller {

	public $authAction;

	public function __construct(){
		parent::__construct();
		$this->load->model('model_alokasi_jam');
	}

	public function index()
	{
		$this->letMeToAccess('view');

		$data['pesan'] = $this->session->flashdata('pesan');
		$data['url_json'] = site_url('alokasi_jam/get_data_json');
		$this->render_view('view_alokasi_jam', $data);
	}

	public function get_data_json(){
		ob_start();
		$this->letMeToAccess('view');

        $data = array();
        $requestData= $_REQUEST;
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $options['order'] = !empty($order) && !empty($columns) ? $columns[$order[0]['column']]['data'] : '';
        $options['mode'] = !empty($order) ? $order[0]['dir']: 'desc';
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $options['offset'] = empty($start) ? 0 : $start;
        $options['limit'] = empty($length) ? 10 : $length;
        $where_like = array();
        if (!empty($requestData['search']['value'])){
            $options['where_like'] = array(
                "hari LIKE '%".$requestData['search']['value']."%'",
                "tahun_mulai LIKE '%".$requestData['search']['value']."%'",
                "tahun_selesai LIKE '%".$requestData['search']['value']."%'"
            );
        }else{
            $options['where_like'] = [];
        }
        $dataOutput = $this->model_alokasi_jam->getListData($options);
        $totalFiltered = $this->model_alokasi_jam->getTotalData($options);
        $totalData = $this->model_alokasi_jam->getTotal();
        $no = $options['offset'] + 1;
        if (!empty($dataOutput)){
            foreach ($dataOutput as $key => $value) {
                $value->no = '<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>';

                $value->aksi = '<div class="hidden-sm hidden-xs action-buttons">
									<a class="green" href="'.site_url('alokasi_jam/edit/'.$value->id_alokasi_jam).'">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>

									<a class="red btn-delete" href="'.site_url('alokasi_jam/delete/'.$value->id_alokasi_jam).'">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

											<li>
												<a href="'.site_url('alokasi_jam/edit/'.$value->id_alokasi_jam).'" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="'.site_url('alokasi_jam/delete/'.$value->id_alokasi_jam).'" class="tooltip-error btn-delete" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>';
            }
        }

        
        $response = array(
            "draw"            => isset($requestData['draw']) ? intval( $requestData['draw'] ) : 0,
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $dataOutput
            );
        echo json_encode($response);
	}

	private function dataConstruct(){
		$data['listTahunAjaran'] = $this->model_alokasi_jam->getTahunAjaran();
		$data['listHari'] = array('senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu');
		$data['listIstirahat'] = array('0' => 'Bukan', '1' => 'Ya');
		$data['post'] = $this->session->flashdata('post');
		$data['pesan'] = $this->session->flashdata('pesan');
		return $data;
	}

	public function add()
	{
		$this->letMeToAccess('add');

		$data = $this->dataConstruct();
		$this->render_view('view_add_alokasi_jam', $data);
	}

	public function do_add()
	{
		$this->letMeToAccess('add');

		if (isset($_POST['batal'])){
			redirect(site_url('alokasi_jam'));
		}
		$hari = $this->input->post('hari');
		$result = true;
		$this->db->trans_begin();

		if (!empty($hari)){
			foreach ($hari as $key => $value) {
				if (!empty($this->input->post('jam_mulai')[$key])){
					$data = [
						'id_ref_tahun_ajaran' => $this->input->post('id_ref_tahun_ajaran'),
						'hari' => $this->input->post('hari')[$key],
						'kode_jam' => $this->input->post('kode_jam')[$key],
						'jam_mulai' => $this->input->post('jam_mulai')[$key],
						'jam_selesai' => $this->input->post('jam_selesai')[$key],
						'is_istirahat' => $this->input->post('is_istirahat')[$key]
					];
					$result = $this->model_alokasi_jam->add($data) && $result;
				}				
			}
		}

		if ($result){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}
		
		if ($result){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses tersimpan']);
			redirect(site_url('alokasi_jam'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal tersimpan']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('alokasi_jam/add'));
		}
	}

	public function edit($id)
	{
		$this->letMeToAccess('edit');

		$data = $this->dataConstruct();
		if (empty($data['post'])){
			$data['post'] = $this->model_alokasi_jam->getDataById($id);
		}
		$this->render_view('view_edit_alokasi_jam', $data);
	}

	public function do_update()
	{
		$this->letMeToAccess('edit');

		if (isset($_POST['batal'])){
			redirect(site_url('alokasi_jam'));
		}
		$data = [
			'kode_jam' => $this->input->post('kode_jam'),
			'jam_mulai' => $this->input->post('jam_mulai'),
			'jam_selesai' => $this->input->post('jam_selesai'),
			'is_istirahat' => $this->input->post('is_istirahat')
		];
		$update = $this->model_alokasi_jam->update($data, ['id_alokasi_jam' => $this->input->post('id_alokasi_jam')]);
		if ($update){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses diubah']);
			redirect(site_url('alokasi_jam'));
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal diubah']);
			$this->session->set_flashdata('post', $this->input->post());
			redirect(site_url('alokasi_jam/edit/'.$this->input->post('id_alokasi_jam')));
		}		
	}

	public function delete($id)
	{
		$this->letMeToAccess('delete');

		$delete = $this->model_alokasi_jam->delete($id);
		if ($delete){
			$this->session->set_flashdata('pesan', ['success', 'Data sukses dihapus']);
		}else{
			$this->session->set_flashdata('pesan', ['danger', 'Data gagal dihapus']);
		}	
		redirect(site_url('alokasi_jam'));
	}
}
