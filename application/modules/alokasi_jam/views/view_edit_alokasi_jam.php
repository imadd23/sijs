<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/ace/plugin/timepicker/jquery-ui-timepicker-addon.css')?>">
<script type="text/javascript" src="<?php echo base_url('assets/ace/plugin/timepicker/jquery-ui-timepicker-addon.js')?>"></script>
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Ubah Alokasi Jam</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>
		
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>

		<?php echo form_open('alokasi_jam/do_update', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tahun Pelajaran </label>
				<div class="col-sm-3">
					<input type="text" name="" class="form-control" value="<?php echo (isset($post)) ? $post['ta'] : '';?>" readonly>
				</div>				
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Hari </label>
				<div class="col-sm-3">
					<input type="text" name="hari" class="form-control" value="<?php echo (isset($post)) ? $post['hari'] : '';?>" readonly>
				</div>				
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Kode Jam </label>
				<div class="col-sm-3">
					<input type="text" name="kode_jam" class="form-control" value="<?php echo (isset($post)) ? $post['kode_jam'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Jam Mulai </label>
				<div class="col-sm-3">
					<input type="text" name="jam_mulai" class="form-control timepicker" value="<?php echo (isset($post)) ? $post['jam_mulai'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Jam Selesai </label>
				<div class="col-sm-3">
					<input type="text" name="jam_selesai" class="form-control timepicker" value="<?php echo (isset($post)) ? $post['jam_selesai'] : '';?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Istirahat ? </label>
				<div class="col-sm-3">
					<?php
					foreach ($listIstirahat as $key => $value) {
						if (isset($post) && $post['is_istirahat'] == $key){
							$selected = 'checked';
						}else{
							$selected = '';
						}

						echo '<input type="radio" name="is_istirahat" value="'.$key.'" '.$selected.'>'.$value.'&nbsp;';
					}
					?>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<input type="hidden" name="id_alokasi_jam" value="<?php echo (isset($post)) ? $post['id_alokasi_jam'] : 0;?>">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.timepicker').timepicker({
			controlType : 'select'
		});
	});
</script>