<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/ace/plugin/timepicker/jquery-ui-timepicker-addon.css')?>">
<script type="text/javascript" src="<?php echo base_url('assets/ace/plugin/timepicker/jquery-ui-timepicker-addon.js')?>"></script>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<h3 class="header smaller lighter blue">Tambah Alokasi Jam</h3>

		<?php if (!empty($pesan)){ ?>
		<div class="alert alert-block alert-<?php echo $pesan[0];?>">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<?php echo $pesan[1];?>
		</div>
		<?php } ?>

		<?php echo form_open('alokasi_jam/do_add', ['class' => 'form-horizontal', 'role' => 'form']);?>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tahun Ajaran </label>

				<div class="col-sm-2">
					<select id="id_ref_tahun_ajaran" name="id_ref_tahun_ajaran" class="form-control input-sm" />
						<?php
						foreach ($listTahunAjaran as $key => $value) {
							if (isset($post) && $post['id_ref_tahun_ajaran'] == $value['id_ref_tahun_ajaran']){
								$selected = 'selected="selected"';
							}else{
								$selected = '';
							}

							echo '<option value="'.$value['id_ref_tahun_ajaran'].'" '.$selected.'>'.$value['ta'].'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Alokasi Jam </label>

				<div class="col-sm-10">
					<table class="table table-striped"> 
						<tr>
							<th class="text-center">Hari</th>
							<th class="text-center col-sm-1">Kode Jam</th>
							<th class="text-center">Jam Mulai</th>
							<th class="text-center">Jam Selesai</th>
							<th class="text-center">Istirahat ?</th>
						</tr>
						<?php if (!empty($listHari)){ 
							foreach ($listHari as $key => $value) {
								?>
								<tr>
									<td class="text-center">
										<label class="label label-success"><?php echo $value;?></label>
										<input type="hidden" name="hari[<?php echo $key;?>]" value="<?php echo $value;?>">
									</td>
									<td class="text-center">
										<input type="text" class="input-sm form-control" name="kode_jam[<?php echo $key;?>]" value="<?php echo (isset($post)) ? $post['kode_jam'][$key] : '';?>">
									</td>
									<td class="text-center">
										<input type="text" class="input-sm timepicker" name="jam_mulai[<?php echo $key;?>]" value="<?php echo (isset($post)) ? $post['jam_mulai'][$key] : '';?>">
									</td class="text-center">
									<td class="text-center">
										<input type="text" class="input-sm timepicker" name="jam_selesai[<?php echo $key;?>]" value="<?php echo (isset($post)) ? $post['jam_selesai'][$key] : '';?>">
									</td>
									<td class="text-center">
										<input type="radio" name="is_istirahat[<?php echo $key;?>]" value="0" checked>Bukan &nbsp;
										<input type="radio" name="is_istirahat[<?php echo $key;?>]" value="1">Ya
									</td>
								</tr>
								<?php
							}

						} ?>
					</table>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-4 col-md-9">
					<button class="btn btn-info" type="submit" name="simpan">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Simpan
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="submit" name="batal">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Batal
					</button>
				</div>
			</div>
		<?php echo form_close();?>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.timepicker').timepicker({
			controlType : 'select'
		});
	});
</script>