<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>SIJS - SMKN 1 Kersana</title>

		<meta name="description" content="Sistem Informasi Jadwal Sekolah SMKN 1 Kersana" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/bootstrap.min.css');?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/font-awesome/4.5.0/css/font-awesome.min.css');?>" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/jquery-ui.min.css');?>" />
		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/fonts.googleapis.com.css');?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace.min.css');?>" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-part2.min.css');?>" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-skins.min.css');?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-rtl.min.css');?>" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo base_url('assets/ace/css/ace-ie.min.css');?>" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<?php echo base_url('assets/ace/js/ace-extra.min.js');?>"></script>

		<!-- HTML5shiv and Respond.js');?> for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo base_url('assets/ace/js/html5shiv.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/respond.min.js');?>"></script>
		<![endif]-->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<?php echo base_url('assets/ace/js/jquery-2.1.4.min.js');?>"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="<?php echo base_url('assets/ace/js/jquery-1.11.3.min.js');?>"></script>
		<![endif]-->

		<!-- jQuery UI -->
		<script src="<?php echo base_url('assets/ace/js/jquery-ui.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/jquery.ui.touch-punch.min.js');?>"></script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url('assets/ace/js/jquery.mobile.custom.min.js');?>'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url('assets/ace/js/bootstrap.min.js');?>"></script>
	</head>

	<body class="skin-2">
		<div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo base_url();?>" class="navbar-brand">
						<small>
							<i class="fa fa-clock-o"></i>
							SIJS-SMKN 1 Kersana
						</small>
					</a>

					<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menus">
						<span class="sr-only">Toggle user menu</span>

						<img src="<?php echo base_url('assets/ace/images/avatars/user.jpg');?>" alt="" />
					</button>

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-menu">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">

						<li class="purple dropdown-modal user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo base_url('assets/ace/images/avatars/avatar2.png');?>" alt="Jason's Photo" />
								<span class="user-info">
									<?php echo $this->session->userdata('nama');?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo site_url('profil');?>">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo site_url('auth/do_logout');?>">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<nav role="navigation" class="navbar-menu pull-left collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ace-icon fa fa-database bigger-110"></i>
								Master Data <i class="ace-icon fa fa-angle-down bigger-110"></i>
							</a>

							<ul class="dropdown-menu dropdown-purple dropdown-caret">
								<?php if ($obj->has_access('ref_tahun_ajaran', 'view')){ ?>
									<li>
										<a href="<?php echo site_url('ref_tahun_ajaran');?>">
											<i class="ace-icon fa fa-cog bigger-110 purple"></i>
											Tahun Ajaran
										</a>
									</li>
								<?php }?>
								
								<?php if ($obj->has_access('ref_jenjang', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('ref_jenjang');?>">
										<i class="ace-icon fa fa-cog bigger-110 purple"></i>
										Referensi Jenjang
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('ref_jurusan', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('ref_jurusan');?>">
										<i class="ace-icon fa fa-cog bigger-110 purple"></i>
										Referensi Jurusan
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('ref_ruang', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('ref_ruang');?>">
										<i class="ace-icon fa fa-cog bigger-110 purple"></i>
										Referensi Ruang
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('ref_mapel', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('ref_mapel');?>">
										<i class="ace-icon fa fa-book bigger-110 purple"></i>
										Referensi Mapel
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('ref_guru', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('ref_guru');?>">
										<i class="ace-icon fa fa-user bigger-110 purple"></i>
										Guru
									</a>
								</li>
								<?php }?>
								
							</ul>
						</li>

						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ace-icon fa fa-file bigger-110"></i>
								Olah Data <i class="ace-icon fa fa-angle-down bigger-110"></i>
							</a>

							<ul class="dropdown-menu dropdown-purple dropdown-caret">
								<?php if ($obj->has_access('detail_kelas', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('detail_kelas');?>">
										<i class="ace-icon fa fa-home bigger-110 purple"></i>
										Kelas
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('alokasi_jam', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('alokasi_jam');?>">
										<i class="ace-icon fa fa-clock-o bigger-110 purple"></i>
										Alokasi Jam
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('guru_mapel', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('guru_mapel');?>">
										<i class="ace-icon fa fa-user bigger-110 purple"></i>
										Guru Mapel
									</a>
								</li>
								<?php }?>
								
								<?php if ($obj->has_access('jadwal_pelajaran', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('jadwal_pelajaran');?>">
										<i class="ace-icon fa fa-calendar bigger-110 purple"></i>
										Jadwal Pelajaran
									</a>
								</li>
								<?php }?>

							</ul>
						</li>

						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ace-icon fa fa-lock bigger-110"></i>
								Hak Akses <i class="ace-icon fa fa-angle-down bigger-110"></i>
							</a>

							<ul class="dropdown-menu dropdown-purple dropdown-caret">
								<?php if ($obj->has_access('group', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('group');?>">
										<i class="ace-icon fa fa-users bigger-110 purple"></i>
										Group User
									</a>
								</li>
								<?php }?>

								<?php if ($obj->has_access('user', 'view')){ ?>
								<li>
									<a href="<?php echo site_url('user');?>">
										<i class="ace-icon fa fa-user bigger-110 purple"></i>
										User
									</a>
								</li>
								<?php }?>
								
							</ul>
						</li>
					</ul>
				</nav>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">

						<!-- <div class="page-header">
							<h1>
								Top Menu Style
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									top menu &amp; navigation
								</small>
							</h1>
						</div> --><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<?php $this->load->view($view, $params);?>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">SMKN 1 Kersana</span>
							SIJS &copy; 2016
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- page specific plugin scripts -->
		<script src="<?php echo base_url('assets/ace/js/jquery.dataTables.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/jquery.dataTables.bootstrap.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/dataTables.buttons.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/buttons.flash.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/buttons.html5.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/buttons.print.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/buttons.colVis.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/dataTables.select.min.js');?>"></script>

		<!-- ace scripts -->
		<script src="<?php echo base_url('assets/ace/js/ace-elements.min.js');?>"></script>
		<script src="<?php echo base_url('assets/ace/js/ace.min.js');?>"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
		</script>
	</body>
</html>
