<?php
/**
*@author: imadd23@yahoo.com
*@version: 01
*@desc: library ini digunakan untuk hak akses pengguna sistem
*/

Class AuthSecure{

	protected $ci;
	protected $openController = ['welcome', 'auth', 'beranda'];
	protected $controller;
	protected $method;
	protected $_authAction;

	public function __construct(){
		parent::__construct();
		$this->ci =& get_instance();
		$this->controller = $this->ci->router->fetch_class();
		$this->method = $this->ci->router->fetch_method();

		// load module twice for get child of action
		/* set the module directory */
		// $path = APPPATH.'controllers/'.CI::$APP->router->directory;
		
		// /* load the controller class */
		// $class = $this->ci->router->module.CI::$APP->config->item('controller_suffix');
		// // Modules::load_file(ucfirst($class), $path);
		
		// /* create and register the new controller */
		// $_controller = ucfirst($class);
		// require_once $path.$class.EXT;
		$obj = Modules::load($this->ci->router->module);
		print_r($obj);die();
		$this->_authAction = (property_exists($controller, 'authAction')) ? $controller->authAction : '';
	}

	public function letMeToAccess(){
		if (!in_array($this->controller, $this->openController)){
			$this->ci->load->library(['session']);
			$role_id = is_null($this->ci->session->userdata('role_id')) ? 0 : $this->ci->session->userdata('role_id');
			$qAuth = $this->ci->db->select('*')->from('auth_role_page')
				->join('auth_page', 'auth_page.page_id = auth_role_page.page_id')
				->join('auth_role', 'auth_role.role_id = auth_role_page.page_id')
				->where(['auth_role_page.role_id' => $role_id, 'page' => $this->controller, 'action' => $this->_authAction])
				->get()->num_rows();
			$hasAuthenticated = ($qAuth > 0) ? true : false;
			if (!$hasAuthenticated){
				die('Sorry, This system restrict your access for this page');
			}
		}		
		return true;
	}
}
?>