/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.24 : Database - penjadwalan_smkn_1_kersana
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penjadwalan_smkn_1_kersana` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `penjadwalan_smkn_1_kersana`;

/*Table structure for table `alokasi_jam` */

DROP TABLE IF EXISTS `alokasi_jam`;

CREATE TABLE `alokasi_jam` (
  `id_alokasi_jam` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ref_tahun_ajaran` int(11) DEFAULT NULL,
  `hari` enum('senin','selasa','rabu','kamis','jumat','sabtu','minggu') DEFAULT NULL,
  `kode_jam` int(11) DEFAULT NULL,
  `jam_mulai` time DEFAULT NULL,
  `jam_selesai` time DEFAULT NULL,
  `is_istirahat` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id_alokasi_jam`),
  KEY `id_ref_tahun_ajaran` (`id_ref_tahun_ajaran`),
  CONSTRAINT `alokasi_jam_ibfk_1` FOREIGN KEY (`id_ref_tahun_ajaran`) REFERENCES `ref_tahun_ajaran` (`id_ref_tahun_ajaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

/*Data for the table `alokasi_jam` */

insert  into `alokasi_jam`(`id_alokasi_jam`,`id_ref_tahun_ajaran`,`hari`,`kode_jam`,`jam_mulai`,`jam_selesai`,`is_istirahat`) values (1,1,'senin',1,'07:00:00','08:10:00','0'),(2,1,'senin',2,'08:10:00','08:45:00','0'),(3,1,'senin',3,'08:45:00','09:20:00','0'),(4,1,'senin',4,'09:20:00','09:55:00','0'),(5,1,'senin',0,'09:55:00','10:10:00','1'),(6,1,'senin',5,'10:10:00','10:45:00','0'),(7,1,'senin',6,'10:45:00','11:20:00','0'),(8,1,'senin',7,'11:20:00','11:55:00','0'),(9,1,'senin',0,'11:55:00','12:25:00','1'),(10,1,'senin',8,'12:25:00','12:55:00','0'),(11,1,'senin',9,'12:55:00','13:30:00','0'),(14,1,'selasa',1,'07:00:00','07:45:00','0'),(15,1,'rabu',1,'07:00:00','07:45:00','0'),(16,1,'kamis',1,'07:00:00','07:45:00','0'),(17,1,'jumat',1,'07:00:00','07:45:00','0'),(18,1,'sabtu',1,'07:00:00','07:45:00','0'),(19,1,'selasa',2,'07:45:00','08:30:00','0'),(20,1,'rabu',2,'07:45:00','08:30:00','0'),(21,1,'kamis',2,'07:45:00','08:30:00','0'),(22,1,'jumat',2,'07:45:00','08:30:00','0'),(23,1,'sabtu',2,'07:45:00','08:30:00','0'),(25,1,'selasa',3,'08:30:00','09:15:00','0'),(26,1,'rabu',3,'08:30:00','09:15:00','0'),(27,1,'kamis',3,'08:30:00','09:15:00','0'),(28,1,'jumat',3,'08:30:00','09:15:00','0'),(29,1,'sabtu',3,'08:30:00','09:15:00','0'),(31,1,'selasa',4,'09:15:00','10:00:00','0'),(32,1,'rabu',4,'09:15:00','10:00:00','0'),(33,1,'kamis',4,'09:15:00','10:00:00','0'),(34,1,'jumat',4,'09:15:00','10:00:00','0'),(35,1,'sabtu',4,'09:15:00','10:00:00','0'),(37,1,'selasa',0,'10:00:00','10:15:00','1'),(38,1,'rabu',0,'10:00:00','10:15:00','1'),(39,1,'kamis',0,'10:00:00','10:15:00','1'),(40,1,'jumat',0,'10:00:00','10:15:00','1'),(41,1,'sabtu',0,'10:00:00','10:15:00','1'),(43,1,'selasa',5,'10:15:00','11:00:00','0'),(44,1,'rabu',5,'10:15:00','11:00:00','0'),(45,1,'kamis',5,'10:15:00','11:00:00','0'),(46,1,'jumat',5,'10:15:00','11:00:00','0'),(47,1,'sabtu',5,'10:15:00','11:00:00','0'),(49,1,'selasa',6,'11:00:00','11:45:00','0'),(50,1,'rabu',6,'11:00:00','11:45:00','0'),(51,1,'kamis',6,'11:00:00','11:45:00','0'),(52,1,'sabtu',6,'11:00:00','11:45:00','0'),(53,1,'selasa',0,'11:45:00','12:15:00','1'),(54,1,'rabu',0,'11:45:00','12:15:00','1'),(55,1,'kamis',0,'11:45:00','12:15:00','1'),(56,1,'selasa',7,'12:15:00','13:00:00','0'),(57,1,'rabu',7,'12:15:00','13:00:00','0'),(58,1,'kamis',7,'12:15:00','13:00:00','0'),(59,1,'selasa',8,'13:00:00','13:45:00','0'),(60,1,'rabu',8,'13:00:00','13:45:00','0'),(61,1,'kamis',8,'13:00:00','13:45:00','0'),(62,1,'selasa',9,'13:45:00','14:30:00','0'),(63,1,'rabu',9,'13:45:00','14:30:00','0'),(64,1,'kamis',9,'13:45:00','14:30:00','0'),(65,1,'selasa',10,'14:30:00','15:15:00','0'),(66,1,'rabu',10,'14:30:00','15:15:00','0'),(67,1,'kamis',10,'14:30:00','15:15:00','0');

/*Table structure for table `auth_page` */

DROP TABLE IF EXISTS `auth_page`;

CREATE TABLE `auth_page` (
  `page_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

/*Data for the table `auth_page` */

insert  into `auth_page`(`page_id`,`page`,`action`) values (1,'guru','add'),(2,'guru','edit'),(3,'guru','delete'),(4,'guru','view'),(5,'ref_tahun_ajaran','view'),(6,'ref_tahun_ajaran','add'),(7,'ref_tahun_ajaran','edit'),(8,'ref_tahun_ajaran','delete'),(9,'ref_jenjang','view'),(10,'ref_jenjang','add'),(11,'ref_jenjang','edit'),(12,'ref_jenjang','delete'),(13,'ref_jurusan','view'),(14,'ref_jurusan','add'),(15,'ref_jurusan','edit'),(16,'ref_jurusan','delete'),(17,'detail_kelas','view'),(18,'detail_kelas','add'),(19,'detail_kelas','edit'),(20,'detail_kelas','delete'),(21,'ref_ruang','view'),(22,'ref_ruang','add'),(23,'ref_ruang','edit'),(24,'ref_ruang','delete'),(25,'ref_mapel','view'),(26,'ref_mapel','add'),(27,'ref_mapel','edit'),(28,'ref_mapel','delete'),(29,'ref_guru','view'),(30,'ref_guru','add'),(31,'ref_guru','edit'),(32,'ref_guru','delete'),(33,'alokasi_jam','view'),(34,'alokasi_jam','add'),(35,'alokasi_jam','edit'),(36,'alokasi_jam','delete'),(37,'guru_mapel','view'),(38,'guru_mapel','add'),(39,'guru_mapel','edit'),(40,'guru_mapel','delete'),(41,'jadwal_pelajaran','view'),(42,'jadwal_pelajaran','add'),(43,'jadwal_pelajaran','edit'),(44,'jadwal_pelajaran','delete'),(45,'group','view'),(46,'group','add'),(47,'group','edit'),(48,'group','delete'),(49,'user','view'),(50,'user','add'),(51,'user','edit'),(52,'user','delete');

/*Table structure for table `auth_role` */

DROP TABLE IF EXISTS `auth_role`;

CREATE TABLE `auth_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `auth_role` */

insert  into `auth_role`(`role_id`,`role_name`) values (1,'admin kurikulum'),(3,'Guru Mapel');

/*Table structure for table `auth_role_page` */

DROP TABLE IF EXISTS `auth_role_page`;

CREATE TABLE `auth_role_page` (
  `role_id` bigint(20) DEFAULT NULL,
  `page_id` bigint(20) DEFAULT NULL,
  UNIQUE KEY `role_id` (`role_id`,`page_id`),
  KEY `role_page` (`page_id`),
  CONSTRAINT `auth_role_page_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `auth_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_role_page_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `auth_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `auth_role_page` */

insert  into `auth_role_page`(`role_id`,`page_id`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,40),(1,41),(1,42),(1,43),(1,44),(1,45),(1,46),(1,47),(1,48),(1,49),(1,50),(1,51),(1,52),(3,41);

/*Table structure for table `det_kelas` */

DROP TABLE IF EXISTS `det_kelas`;

CREATE TABLE `det_kelas` (
  `Urut` int(11) NOT NULL,
  `id_kelas` varchar(15) DEFAULT NULL,
  `Nis` int(11) DEFAULT NULL,
  PRIMARY KEY (`Urut`),
  KEY `id_kelas` (`id_kelas`),
  CONSTRAINT `det_kelas_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `det_kelas` */

/*Table structure for table `detail_kelas` */

DROP TABLE IF EXISTS `detail_kelas`;

CREATE TABLE `detail_kelas` (
  `id_detail_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_ref_tahun_ajaran` int(11) DEFAULT NULL,
  `id_ref_jenjang` int(11) DEFAULT NULL,
  `id_ref_jurusan` int(11) DEFAULT NULL,
  `nama_kelas` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_detail_kelas`),
  UNIQUE KEY `id_ref_tahun_ajaran` (`id_ref_tahun_ajaran`,`id_ref_jenjang`,`id_ref_jurusan`,`nama_kelas`),
  KEY `id_ref_jenjang` (`id_ref_jenjang`),
  KEY `id_ref_jurusan` (`id_ref_jurusan`),
  CONSTRAINT `detail_kelas_ibfk_1` FOREIGN KEY (`id_ref_jenjang`) REFERENCES `ref_jenjang` (`id_ref_jenjang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_kelas_ibfk_2` FOREIGN KEY (`id_ref_jurusan`) REFERENCES `ref_jurusan` (`id_ref_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_kelas_ibfk_3` FOREIGN KEY (`id_ref_tahun_ajaran`) REFERENCES `ref_tahun_ajaran` (`id_ref_tahun_ajaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `detail_kelas` */

insert  into `detail_kelas`(`id_detail_kelas`,`id_ref_tahun_ajaran`,`id_ref_jenjang`,`id_ref_jurusan`,`nama_kelas`) values (1,1,1,1,'A'),(2,1,1,1,'B'),(3,1,1,1,'C'),(4,1,1,1,'D'),(5,1,1,1,'E'),(14,1,1,2,'A'),(15,1,1,2,'B'),(20,1,1,6,'A'),(21,1,1,6,'B'),(6,1,2,1,'A'),(7,1,2,1,'B'),(8,1,2,1,'C'),(9,1,2,1,'D'),(10,1,2,1,'E'),(16,1,2,2,'A'),(17,1,2,2,'B'),(22,1,2,6,'A'),(23,1,2,6,'B'),(11,1,3,1,'A'),(12,1,3,1,'B'),(13,1,3,1,'C'),(18,1,3,2,'A'),(19,1,3,2,'B');

/*Table structure for table `guru` */

DROP TABLE IF EXISTS `guru`;

CREATE TABLE `guru` (
  `id_guru` varchar(15) NOT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `Nip` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `guru` */

insert  into `guru`(`id_guru`,`Nama`,`Nip`) values ('AS','Arif Sugiharto, S.Pd','0'),('IN','Drs. Asikin, M.Pd','0'),('JB','Jubaedi, S.Pd,. M.Pd','0'),('RD','Radiman, M.Pd','0'),('SA','Sabilia Asya Gumelar, S. ','0'),('SG','Sugiarto, S.Pd','0'),('SM','Drs. H. Samsudin, M.Pd','0'),('SY','Syaimah','0');

/*Table structure for table `guru_mapel` */

DROP TABLE IF EXISTS `guru_mapel`;

CREATE TABLE `guru_mapel` (
  `id_guru_mapel` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ref_tahun_ajaran` int(11) DEFAULT NULL,
  `id_guru` varchar(15) DEFAULT NULL,
  `id_ref_mapel` bigint(20) DEFAULT NULL,
  `warna` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_guru_mapel`),
  KEY `id_ref_tahun_ajaran` (`id_ref_tahun_ajaran`),
  KEY `id_guru` (`id_guru`),
  KEY `id_ref_mapel` (`id_ref_mapel`),
  CONSTRAINT `guru_mapel_ibfk_1` FOREIGN KEY (`id_ref_tahun_ajaran`) REFERENCES `ref_tahun_ajaran` (`id_ref_tahun_ajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `guru_mapel_ibfk_2` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `guru_mapel_ibfk_3` FOREIGN KEY (`id_ref_mapel`) REFERENCES `ref_mapel` (`id_ref_mapel`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `guru_mapel` */

insert  into `guru_mapel`(`id_guru_mapel`,`id_ref_tahun_ajaran`,`id_guru`,`id_ref_mapel`,`warna`) values (1,1,'SA',5,'#ff0000'),(2,1,'AS',3,'#e36c09'),(3,1,'IN',1,'#8064a2'),(4,1,'JB',4,'#00b050'),(5,1,'RD',2,'#938953'),(6,1,'SA',4,'#92d050'),(7,1,'SA',3,'#953734'),(8,1,'SY',2,'#17365d');

/*Table structure for table `jadwal` */

DROP TABLE IF EXISTS `jadwal`;

CREATE TABLE `jadwal` (
  `id_jadwal` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_alokasi_jam` bigint(20) DEFAULT NULL,
  `id_detail_kelas` int(11) DEFAULT NULL,
  `id_guru_mapel` bigint(20) DEFAULT NULL,
  `id_ref_ruang` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`),
  KEY `id_alokasi_jam` (`id_alokasi_jam`),
  KEY `id_detail_kelas` (`id_detail_kelas`),
  KEY `id_guru_mapel` (`id_guru_mapel`),
  KEY `id_ref_ruang` (`id_ref_ruang`),
  CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_alokasi_jam`) REFERENCES `alokasi_jam` (`id_alokasi_jam`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`id_detail_kelas`) REFERENCES `detail_kelas` (`id_detail_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`id_guru_mapel`) REFERENCES `guru_mapel` (`id_guru_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_ibfk_4` FOREIGN KEY (`id_ref_ruang`) REFERENCES `ref_ruang` (`id_ref_ruang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `jadwal` */

insert  into `jadwal`(`id_jadwal`,`id_alokasi_jam`,`id_detail_kelas`,`id_guru_mapel`,`id_ref_ruang`) values (13,1,1,1,1),(14,2,1,1,2);

/*Table structure for table `jadwal_orig` */

DROP TABLE IF EXISTS `jadwal_orig`;

CREATE TABLE `jadwal_orig` (
  `Urut` int(11) NOT NULL,
  `Jam` int(11) DEFAULT NULL,
  `Hari` varchar(15) DEFAULT NULL,
  `id_kelas` varchar(15) DEFAULT NULL,
  `id_ruang` varchar(15) DEFAULT NULL,
  `kd_ampu` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Urut`),
  KEY `id_kelas` (`id_kelas`),
  KEY `id_ruang` (`id_ruang`),
  KEY `kd_ampu` (`kd_ampu`),
  CONSTRAINT `jadwal_orig_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_orig_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_orig_ibfk_3` FOREIGN KEY (`kd_ampu`) REFERENCES `pengampu` (`kd_ampu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jadwal_orig` */

/*Table structure for table `jurusan` */

DROP TABLE IF EXISTS `jurusan`;

CREATE TABLE `jurusan` (
  `kd_jurusan` varchar(15) NOT NULL,
  `jurusan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kd_jurusan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jurusan` */

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kd_kategori` varchar(15) NOT NULL,
  `Ket` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kd_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `kategori` */

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` varchar(15) NOT NULL,
  `id_guru` varchar(15) DEFAULT NULL,
  `Ta` varchar(15) DEFAULT NULL,
  `Kelas` varchar(15) DEFAULT NULL,
  `kd_jurusan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_kelas`),
  KEY `id_guru` (`id_guru`),
  KEY `kd_jurusan` (`kd_jurusan`),
  CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`kd_jurusan`) REFERENCES `jurusan` (`kd_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `kelas` */

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id_logkar` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_guru` varchar(15) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_logkar`),
  UNIQUE KEY `email` (`email`),
  KEY `id_guru` (`id_guru`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `login_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `login_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `auth_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `login` */

insert  into `login`(`id_logkar`,`id_guru`,`email`,`password`,`role_id`) values (1,'SA','lia@gmail.com','21232f297a57a5a743894a0e4a801fc3',1),(2,'IN','asikin@mail.com','827ccb0eea8a706c4c34a16891f84e7b',3);

/*Table structure for table `mapel` */

DROP TABLE IF EXISTS `mapel`;

CREATE TABLE `mapel` (
  `kd_mapel` varchar(15) NOT NULL,
  `kd_kategori` varchar(15) DEFAULT NULL,
  `Nama_mapel` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`kd_mapel`),
  KEY `kd_kategori` (`kd_kategori`),
  CONSTRAINT `mapel_ibfk_1` FOREIGN KEY (`kd_kategori`) REFERENCES `kategori` (`kd_kategori`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mapel` */

/*Table structure for table `pengampu` */

DROP TABLE IF EXISTS `pengampu`;

CREATE TABLE `pengampu` (
  `kd_ampu` varchar(15) NOT NULL,
  `id_guru` varchar(15) DEFAULT NULL,
  `kd_mapel` varchar(15) DEFAULT NULL,
  `ta` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`kd_ampu`),
  KEY `id_guru` (`id_guru`),
  KEY `kd_mapel` (`kd_mapel`),
  CONSTRAINT `pengampu_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pengampu_ibfk_2` FOREIGN KEY (`kd_mapel`) REFERENCES `mapel` (`kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pengampu` */

/*Table structure for table `ref_jenjang` */

DROP TABLE IF EXISTS `ref_jenjang`;

CREATE TABLE `ref_jenjang` (
  `id_ref_jenjang` int(11) NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_ref_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ref_jenjang` */

insert  into `ref_jenjang`(`id_ref_jenjang`,`jenjang`) values (1,'X'),(2,'XI'),(3,'XII');

/*Table structure for table `ref_jurusan` */

DROP TABLE IF EXISTS `ref_jurusan`;

CREATE TABLE `ref_jurusan` (
  `id_ref_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(150) DEFAULT NULL,
  `nama_singkat_jurusan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_ref_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `ref_jurusan` */

insert  into `ref_jurusan`(`id_ref_jurusan`,`nama_jurusan`,`nama_singkat_jurusan`) values (1,'Teknik Otomotif','TKR'),(2,'Jasa Boga','JB'),(3,'Tata Boga','TB'),(4,'Multimedia','MM'),(5,'Teknik Pengelasan','TL'),(6,'Akuntansi','AK');

/*Table structure for table `ref_mapel` */

DROP TABLE IF EXISTS `ref_mapel`;

CREATE TABLE `ref_mapel` (
  `id_ref_mapel` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_mapel` varchar(10) DEFAULT NULL,
  `nama_mapel` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ref_mapel`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `ref_mapel` */

insert  into `ref_mapel`(`id_ref_mapel`,`kode_mapel`,`nama_mapel`) values (1,'BJ','Bahasa Jawa'),(2,'OR','Olah Raga'),(3,'KIM','Kimia'),(4,'PAI','Pendidikan Agama Islam'),(5,'SIM DIG','Simulai Digital');

/*Table structure for table `ref_ruang` */

DROP TABLE IF EXISTS `ref_ruang`;

CREATE TABLE `ref_ruang` (
  `id_ref_ruang` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(25) DEFAULT NULL,
  `nama_kelas` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ref_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `ref_ruang` */

insert  into `ref_ruang`(`id_ref_ruang`,`nama_ruang`,`nama_kelas`) values (1,'R.1','Moving Boga'),(2,'R.2','Moving Boga'),(3,'R.3','Moving Boga'),(4,'R.4','Moving Boga'),(5,'R.5','Moving Boga'),(6,'R.6','Moving Akuntansi'),(7,'R.7','Moving Akuntansi'),(8,'R.8','Moving Akuntansi');

/*Table structure for table `ref_tahun_ajaran` */

DROP TABLE IF EXISTS `ref_tahun_ajaran`;

CREATE TABLE `ref_tahun_ajaran` (
  `id_ref_tahun_ajaran` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_mulai` year(4) DEFAULT NULL,
  `tahun_selesai` year(4) DEFAULT NULL,
  `aktif` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id_ref_tahun_ajaran`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ref_tahun_ajaran` */

insert  into `ref_tahun_ajaran`(`id_ref_tahun_ajaran`,`tahun_mulai`,`tahun_selesai`,`aktif`) values (1,2016,2017,'1'),(2,2017,2018,'0');

/*Table structure for table `ruang` */

DROP TABLE IF EXISTS `ruang`;

CREATE TABLE `ruang` (
  `id_ruang` varchar(15) NOT NULL,
  `id_kelas` varchar(15) DEFAULT NULL,
  `nama_ruang` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_ruang`),
  KEY `id_kelas` (`id_kelas`),
  CONSTRAINT `ruang_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ruang` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
